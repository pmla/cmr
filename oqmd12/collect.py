from collections import defaultdict

from ase.db import connect
from ase.phasediagram import PhaseDiagram


nm = connect('nm.db')
fm = connect('fm.db')
nmu = connect('nmu.db')
fmu = connect('fmu.db')

gpaw = connect('gpaw-in.db')
db1 = connect('oqmd-1.db')
db2 = connect('oqmd-2.db')

db = connect('oqmd12.db')

u_atoms = {'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Nb', 'Mo',
           'Tc', 'Ru', 'Rh', 'Pd', 'Cd', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt'}
mag_elements = {'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn',
                'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In',
                'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl'}


def collect(db, nm, fm, u=False, refs=None, formula2id=None, F1=None):
    refs = refs or {}
    rows1 = []
    F1 = F1 or {}
    for row in gpaw.select():
        if u and not any(symbol in u_atoms for symbol in row.symbols):
            continue
        try:
            nrow = nm.get(formula=row.formula)
        except KeyError:
            print('NM:', row.formula)
            nrow = None
        else:
            if nrow.get('failed'):
                nrow = None
        if any(symbol in mag_elements for symbol in row.symbols):
            try:
                frow = fm.get(formula=row.formula)
            except KeyError:
                print('FM:', row.formula)
                frow = None
            else:
                if frow.get('failed') or abs(frow.get('magmom', 0.0)) < 0.05:
                    frow = None
        else:
            frow = None

        emag = None

        if nrow is not None:
            if frow is not None:
                emag = (nrow.energy - frow.energy) / row.natoms
                if emag > 0.0:
                    row = frow
                else:
                    row = nrow
            else:
                row = nrow
        else:
            if frow is not None:
                row = frow
            else:
                print('NM+FM:', row.formula)
                continue

        count = row.count_atoms()
        ns = len(count)
        if ns == 1:
            refs[row.symbols[0]] = row.energy / row.natoms
            F1[row.symbols[0]] = row.formula
        rows1.append((ns, count, emag, row))

    rows2 = []
    combinations = defaultdict(list)
    for ns, count, emag, row in rows1:
        de = (row.energy -
              sum(n * refs.get(symbol, float('nan'))
                  for symbol, n in count.items())) / row.natoms
        ab = tuple(sorted(count))
        if ns == 2:
            combinations[ab].append((row.formula, de * row.natoms))
        rows2.append((de, ns, emag, row, ab))

    hulls = {}
    for ab, abrefs in combinations.items():
        abrefs = abrefs + [(F1[x], 0.0) for x in ab]
        try:
            pd = PhaseDiagram(abrefs, verbose=False)
        except ValueError:
            print(ab, abrefs)
            continue
        hulls[ab] = ([x for x, e in abrefs],
                     pd.points, pd.hull, pd.simplices, pd.symbols)

    rows3 = sorted(rows2, key=lambda x: x[0])
    id = len(db)
    formula2id = formula2id or {}
    for de, ns, emag, row, ab in rows3:
        id += 1
        if ns == 1:
            formula2id[ab[0]] = id
        else:
            formula2id[row.formula] = id

    for de, ns, emag, row, ab in rows3:
        kvp = row.key_value_pairs
        if emag is not None:
            kvp['emag'] = emag
        if 1:
            if ns == 1:
                r = db1.get(formula=row.formula)
            else:
                r = db2.get(formula=row.formula)
            for k in ['bandgap', 'energy_pa',
                      'stability', 'delta_e']:
                kvp['oqmd_' + k] = r.get(k)
            kvp['oqmd_id'] = r.oqmd_id
        if ns == 2:
            data = {}
            if ab in hulls:
                formulas, points, onhull, simplices, symbols = hulls[ab]
                data['hull'] = {
                    'formulas': formulas,
                    'x': points[:, 1],
                    'y': points[:, 2],
                    'onhull': onhull,
                    'lines': simplices,
                    'symbols': symbols}
        else:
            data = None
        uid = row.formula
        if u:
            uid += '-U'
        db.write(row.toatoms(), ns=ns, de=de, u=u, uid=uid, data=data,
                 **{k: v for k, v in kvp.items() if v is not None})

    return refs, formula2id, F1


with db:
    refs, formula2id, F1 = collect(db, nm, fm)
for symbol in u_atoms:
    refs.pop(symbol, None)
print('U' * 70)
collect(db, nmu, fmu, u=True, refs=refs, formula2id=formula2id, F1=F1)
