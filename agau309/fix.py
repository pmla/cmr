import os
from ase.db import connect
from ase.calculators.singlepoint import SinglePointCalculator

os.environ['USER'] = 'pmla'

db1 = connect('sorted.db')
db2 = connect('agau309.db')

for row in db1.select():
    atoms = row.toatoms()
    atoms.cell[:] = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    atoms.calc = SinglePointCalculator(atoms, energy=row.E)
    atoms.calc.name = 'asap'
    db2.write(atoms)
