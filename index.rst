Projects
========


.. list-table::
   :widths: 1 2

   * - .. image:: images/mixdim.png
          :align: center
          :height: 124px
          :target: mixdim/mixdim.html

     - | :ref:`mixdim`
       | Identification of spatially connected components of materials and a measure of the degree of “1D-ness”, “2D-ness” etc. for each component of materials in the Inorganic Crystal Structure Database (ICSD)

   * - .. image:: images/a2bcx4.png
          :align: center
          :height: 124px
          :target: a2bcx4/a2bcx4.html

     - | :ref:`a2bcx4`
       | Promising Quaternary Chalcogenides as High Band Gap Semiconductors for Tandem Photoelectrochemical Water Splitting Devices: A Computational Screening Approach

   * - .. image:: images/agau309.png
          :align: center
          :height: 124px
          :target: agau309/agau309.html

     - | :ref:`agau309`
       | Ground-state orderings in icosahedral Ag-Au nanoparticles.  Structures at every concentration of a 309-atom Mackay icosahedron, obtained using exact solution of a cluster expansion model.

   * - .. image:: images/pv_pec_oqmd.png
          :align: center
          :height: 124px
          :target: pv_pec_oqmd/pv_pec_oqmd.html

     - | :ref:`pv_pec_oqmd`
       | High-Throughput Computational Assessment of Previously Synthesized Semiconductors for Photovoltaic and Photoelectrochemical Devices.

   * - .. image:: images/oqmd12.png
          :align: center
          :height: 124px
          :target: oqmd12/oqmd12.html
     - | :ref:`oqmd12`
       | GPAW calculations for 1 and 2 component systems from OQMD_ database.  For convex-hull calculations.

   * - .. image:: images/abs3.png
          :align: center
          :height: 124px
          :target: abs3/abs3.html

     - | :ref:`abs3`
       | Sulfide perovskites for solar energy conversion applications: computational screening and synthesis of the selected compound LaYS3.

   * - .. image:: images/adsorption.png
          :align: center
          :width: 189px
          :height: 124px
          :target: adsorption/adsorption.html
     - | :ref:`adsorption`
       | Adsorption energies of 200 reactions involving 8 different adsorbates on 25 different surfaces at full coverage employing different DFT XC functionals as well as many-body perturbation theory within the random phase approximation. Surface energies of the 25 transition metals are compared as well.

   * - .. image:: images/solar.png
          :align: center
          :height: 124px
          :target: solar/solar.html
     - | :ref:`solar`
       | This database contains the Kohn-Sham (B3LYP) HOMO, LUMO, HOMO-LUMO gap, singlet-triplet gap of organic donor-acceptor molecules. The database also contains tight-binding extrapolated HOMO-LUMO gap for polymers and Voc for PCBM blended solar cell with the corresponding molecule.

   * - .. image:: images/abx2.png
          :align: center
          :height: 124px
          :target: abx2/abx2.html
     - | :ref:`abx2`
       | Database contains chalcopyrite, kesterite, and wurtzite polymorphs of II–IV–V2 and III–III–V2 materials investigated for light absorption in the visible range. The data set includes stability information, band gaps, and carrier effective masses.

   * - .. image:: images/c2db.png
          :align: center
          :height: 90px
          :target: c2db/c2db.html
     - | :ref:`c2db`
       | The database contains structural, thermodynamic, elastic, electronic, magnetic, and optical properties of around 2000 two-dimensional materials distributed over more than 30 different crystal structures. The properties are calculated by DFT and many-body perturbation theory (G0W0 and the Bethe-Salpeter Equation for around 250 materials).

   * - .. image:: images/c2dm.png
          :align: center
          :height: 124px
          :target: c2dm/c2dm.html
     - | :ref:`c2dm`
       | Structural and electronic properties calculated with different DFT XC functionals and G0W0 of a large range of 2D materials. At the moment the database contains 216 transition-metal oxides and chalcocogenides.

   * - .. image:: images/vdwh.png
          :align: center
          :width: 230px
          :target: vdwh/vdwh.html
     - | :ref:`vdwh`
       | The dielectric building blocks of 51 transition metal dichalcogenides, hexagonal boron nitride, and graphene is available from this database. The results are used to calculate the dielectric function of van der Waals heterostructures, using the Quantum Electrostratic Heterostructure (QEH) model.

   * - .. image:: images/organometal.png
          :align: center
          :width: 189px
          :height: 124px
          :target: organometal/organometal.html
     - | :ref:`organometal`
       | We have performed electronic structure calculations of 240 perovskites composed of Cs, CH3NH3, and HC(NH2)2 as A-cation, Sn and Pb as B-ion, and a combination of Cl, Br, and I as anions.

   * - .. image:: images/dssc.png
          :align: center
          :width: 189px
          :height: 124px
          :target: dssc/dssc.html
     - | :ref:`dssc`
       | We present a computational screening study of more than 12,000 porphyrin-based dyes obtained by modifying the porphyrin backbone (metal center and axial ligands), substituting hydrogen by fluorine, and adding different side and anchoring groups.

   * - .. image:: images/mp_gllbsc.png
          :align: center
          :width: 189px
          :height: 124px
          :target: mp_gllbsc/mp_gllbsc.html
     - | :ref:`mp_gllbsc`
       | Electronic bandgap calculations are presented for 2400 experimentally known materials from the Materials Project database and the bandgaps, obtained with different types of functionals within density functional theory and (partial) self-consistent GW approximation, are compared for 20 randomly chosen compounds forming an unconventional set of ternary and quaternary materials.

   * - .. image:: images/cubic_perovskites.png
          :align: center
          :width: 189px
          :height: 124px
          :target: cubic_perovskites/cubic_perovskites.html
     - | :ref:`cubic_perovskites`
       | We perform computational screening of around 19 000 oxides, oxynitrides, oxysulfides, oxyfluorides, and oxyfluoronitrides in the cubic perovskite structure with photoelectrochemical cell applications in mind.

   * - .. image:: images/low_symmetry_perovskites.png
          :align: center
          :width: 189px
          :height: 124px
          :target: low_symmetry_perovskites/low_symmetry_perovskites.html
     - | :ref:`low_symmetry_perovskites`
       | We have used density functional theory (DFT) calculations to investigate 300 oxides and oxynitrides in the Ruddlesden–Popper phase of the layered perovskite structure.

   * - .. image:: images/absorption_perovskites.png
          :align: center
          :width: 189px
          :height: 124px
          :target: absorption_perovskites/absorption_perovskites.html
     - | :ref:`absorption_perovskites`
       | We calculate the optical properties of a set of oxides, oxynitrides, and organometal halide cubic and layered perovskites with a bandgap in the visible part of the solar spectrum.

   * - .. image:: images/funct_perovskites.png
          :align: center
          :width: 189px
          :height: 124px
          :target: funct_perovskites/funct_perovskites.html
     - | :ref:`funct_perovskites`
       | We investigate the band gaps and optical spectra of functional perovskites composed of layers of the two cubic perovskite semiconductors BaSnO3-BaTaO2N and LaAlO3-LaTiO2N.

   * - .. image:: images/adsorption.png
          :align: center
          :width: 189px
          :height: 124px
          :target: surfaces/surfaces.html
     - | :ref:`surfaces`
       | Surfaces energies of 25 surfaces employing different DFT XC functionals as well as many-body perturbation theory within the random phase approximation.

   * - .. image:: images/beef.png
          :align: center
          :width: 189px
          :height: 124px
          :target: beef/beef.html
     - | :ref:`beef`
       | We present a general-purpose meta-generalized gradient approximation (MGGA) exchange-correlation functional generated within the Bayesian error estimation functional framework.

   * - .. image:: images/catapp.png
          :align: center
          :width: 189px
          :height: 124px
          :target: catapp/catapp.html
     - | :ref:`catapp1`
       | CatApp data: Calculated reaction and activation energies for elementary coupling reactions occurring on metal surfaces

   * - .. image:: images/dcdft.png
          :align: center
          :width: 189px
          :height: 124px
          :target: dcdft/dcdft.html
     - | :ref:`dcdft`
       | Benchmark: codes precision measured using the database of bulk systems from http://molmod.ugent.be/DeltaCodesDFT.

   * - .. image:: images/tmfp06d.png
          :align: center
          :width: 189px
          :height: 124px
          :target: https://cmrdb.fysik.dtu.dk/tmfp06d
     - | :ref:`tmfp06d`
       | Benchmark: the performance of semilocal and hybrid density functionals in 3d transition-metal chemistry (reproducing published results).

   * - .. image:: images/fcc111.png
          :align: center
          :width: 189px
          :height: 124px
          :target: https://cmrdb.fysik.dtu.dk/fcc111
     - | :ref:`fcc111`
       | Benchmark: adsorption energy of atomic oxygen and carbon on fcc111.

   * - .. image:: images/compression.png
          :align: center
          :width: 189px
          :height: 124px
          :target: https://cmrdb.fysik.dtu.dk/compression
     - | :ref:`compression`
       | Benchmark: compression energies of bulk fcc and rocksalt.

   * - .. image:: images/gbrv.png
          :align: center
          :width: 189px
          :height: 124px
          :target: gbrv/gbrv.html
     - | :ref:`gbrv`
       | Benchmark: Pseudopotentials for high-throughput DFT calculations (reproducing published results).

   * - .. image:: images/g2.png
          :align: center
          :width: 189px
          :height: 124px
          :target: https://cmrdb.fysik.dtu.dk/g2
     - | :ref:`g2`
       | Benchmark: PBE atomization energies and structures of the G2/97 set of molecules.


.. _OQMD: http://oqmd.org/

.. toctree::
    :hidden:

    mixdim/mixdim
    a2bcx4/a2bcx4
    agau309/agau309
    pv_pec_oqmd/pv_pec_oqmd
    oqmd12/oqmd12
    abs3/abs3
    adsorption/adsorption
    solar/solar
    abx2/abx2
    c2db/c2db
    c2dm/c2dm
    vdwh/vdwh
    organometal/organometal
    dssc/dssc
    mp_gllbsc/mp_gllbsc
    cubic_perovskites/cubic_perovskites
    low_symmetry_perovskites/low_symmetry_perovskites
    absorption_perovskites/absorption_perovskites
    funct_perovskites/funct_perovskites
    surfaces/surfaces
    beef/beef
    catapp/catapp
    dcdft/dcdft
    tmfp06d/tmfp06d
    fcc111/fcc111
    compression/compression
    gbrv/gbrv
    g2/g2


All CMR databases are licensed under a
`Creative Commons Attribution-ShareAlike 4.0
International License <http://creativecommons.org/licenses/by-sa/4.0/>`_.

.. image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
    :target: http://creativecommons.org/licenses/by-sa/4.0/
    :alt: Creative Commons License


What is CMR?
------------

.. toctree::

    about


Older CMR-projects
------------------

* `DF for surface science
  <https://cmr1.fysik.dtu.dk/cmr/var/view/BEEF-vdW/index.php>`_
* `One- and Two-photon Water Splitting and Transparent Shielding
  <https://cmr1.fysik.dtu.dk/cmr/var/view/tandem/index.php>`_
* `Reversible H storage
  <https://cmr1.fysik.dtu.dk/cmr/var/view/10.1063_1.3148892/index.php>`_
* `Stability and band gaps
  <https://cmr1.fysik.dtu.dk/cmr/var/view/10.1039_C1EE02717D/index.php>`_


Other links
-----------

* `Browse all databases: <http://cmrdb.fysik.dtu.dk>`_
* `ASE-DB: <https://wiki.fysik.dtu.dk/ase/ase/db/db.html>`_
* :ref:`genindex`
* :ref:`search`


.. toctree::
    :maxdepth: 1

    server
