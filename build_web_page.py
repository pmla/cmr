"""Script for server that builds the CMR web-page continuously.

Initial setup::

    cd ~
    python3 -m venv cmr-web-page
    cd cmr-web-page
    . bin/activate
    pip install sphinx-rtd-theme sphinx matplotlib scipy
    git clone http://gitlab.com/ase/ase.git
    cd ase
    pip install -U .
    cd ..
    git clone http://gitlab.com/camd/cmr.git

Crontab::

    WEB_PAGE_FOLDER=/tmp
    CMD="python build_web_page.py"
    10 20 * * * cd ~/cmr-web-page/cmr; . ../bin/activate; $CMD > ../cmr.out

"""
from __future__ import print_function
import os
import os.path as op
import shutil
import subprocess
import sys

from downloads import downloads


cmds1 = """\
pip install --upgrade pip
touch ../cmr-web-page.lock
cd ../ase; git pull -q &> /dev/null; pip install -U .
git clean -fdx
git pull -q &> /dev/null
mkdir images"""

cmds2 = """\
sphinx-build -b html -d build/doctrees . build/html
mv build/html cmr-web-page
tar -czf tmp.tar.gz cmr-web-page
cp tmp.tar.gz {0}
mv {0}/tmp.tar.gz {0}/cmr-web-page.tar.gz"""

cmds2 = cmds2.format(os.environ['WEB_PAGE_FOLDER'])

if os.path.isfile('../cmr-web-page.lock'):
    sys.exit('Locked')

try:
    for cmd in cmds1.splitlines():
        subprocess.check_call(cmd, shell=True)

    for dir, names in downloads:
        for name in names:
            path = op.join(dir, name)
            shutil.copy(op.join('..', 'downloads', name), path)

    for cmd in cmds2.splitlines():
        subprocess.check_call(cmd, shell=True)

finally:
    os.remove('../cmr-web-page.lock')
