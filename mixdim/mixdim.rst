.. _mixdim:

Definition of a scoring parameter to identify low-dimensional materials components
==================================================================================

.. container:: article

    Peter Mahler Larsen, Mohnish Pandey, Mikkel Strange, and Karsten W. Jacobsen

    `Definition of a scoring parameter to identify low-dimensional materials
    components`__

    arXiv:1808.02114

    __ https://arxiv.org/abs/1808.02114

The data can be downloaded or browsed online:

* Download data: :download:`mixdim.db`
* `Browse data <https://cmrdb.fysik.dtu.dk/mixdim/>`_


.. contents::

Brief description
=================

The analyses of two databases are available here: the Inorganic Crystal
Structure Database (`ICSD <https://www2.fiz-karlsruhe.de/icsd_home.html>`_)
and the Crystallography Open Database (`COD
<https://www.crystallography.net/cod/>`_). The COD database contains the full
structures, dimensionality scores, and publication information, for every
structure with at most 200 atoms. For copyright reasons, the atomic positions
of the ICSD entries cannot be shown here; only the formulae, ICSD code, and
dimensionality scores are shown.




Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Example
-------

.. image:: mixdims.png
.. literalinclude:: mixdim.py
