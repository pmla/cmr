title = 'Benchmark: compression energies of bulk fcc and rocksalt'

default_columns = ['id', 'age', 'formula', 'energy', 'pbc', 'volume', 'charge', 'mass', 'magmom', 'name', 'relativistic']
