from c2db.makeit import get_folders
import os

folders = get_folders()

for name in ['c2db_lw.db', 'c2db.db']:
    name2 = name.split('.')[0] + '-combined-new.db'
    for folder in folders:
        os.system('ase db ../{}/{} -i {}'.format(folder, name, name2))
