"""RMSD calculation for 2D monolayers

Author: Peter Larsen
Date: August 2017
"""

import warnings
import numpy as np
from scipy.spatial.distance import cdist
from scipy.optimize import linear_sum_assignment
from scipy.linalg import polar
from ase import Atoms


def verify_fractional_coordinates(f):
    assert (f[:, :2] >= 0).all()  # Z coordinates can lie outside the cell.
    assert (f[:, :2] < 1).all()


def get_absolute_positions(intermediate_cell, f):
    p = np.dot(f, intermediate_cell)
    m = np.mean(p, axis=0)
    p -= m
    return p, m


def get_neighboring_cells(cell):
    """The first two neighbour squares are investigated.  This should
    be good enough for all but the most extremely distorted cells.
    """
    lim = 2
    nbr_cells = [(i, j) for i in range(-lim, lim + 1)
                 for j in range(-lim, lim + 1)]
    return nbr_cells


def best_alignment(intermediate_cell,
                   f0, f1,
                   numbers0, numbers1,
                   zscale=False):
    verify_fractional_coordinates(f0)
    verify_fractional_coordinates(f1)

    # sort atoms by atomic numbers
    num_atoms = len(numbers0)
    indices0 = np.argsort(numbers0)
    indices1 = np.argsort(numbers1)
    numbers = numbers0[indices0]
    f0 = f0[indices0]
    f1 = f1[indices1]

    # get absolute positions in intermediate cell frame of reference
    p0, m0 = get_absolute_positions(intermediate_cell, f0)
    p1, m1 = get_absolute_positions(intermediate_cell, f1)

    # do optimal scaling if desired
    if zscale:
        sopt = np.sum(p1[:, 2]**2) / np.sum(p0[:, 2]**2)
        sopt = np.sqrt(np.sqrt(sopt))
        p0[:, 2] *= sopt
        p1[:, 2] /= sopt

    elements = np.unique(numbers)
    eindices = [np.where(numbers == element)[0] for element in elements]

    shift = intermediate_cell[:2]
    nbr_cells = get_neighboring_cells(intermediate_cell)
    p1_nbrs = [np.concatenate([p1[indices] + np.dot(shift.T, nbr)
                               for nbr in nbr_cells]) for indices in eindices]

    xindices = np.argsort(f0[:, 0])
    yindices = np.argsort(f0[:, 1])

    best = (float('inf'), None, None)
    for j in yindices:
        for i in xindices:

            perms = []
            zsum = 0
            for indices, pmatch in zip(eindices, p1_nbrs):

                num = len(indices)
                dist = cdist(p0[indices], pmatch, 'sqeuclidean')
                dist = np.min(dist.reshape((num, len(nbr_cells), num)), axis=1)
                perm = list(zip(*linear_sum_assignment(dist)))
                perms += [perm]
                z = sum([dist[e] for e in perm])
                zsum += z

            if zsum < best[0]:
                best = (zsum, perms, np.copy(p0))

            p0[i] += shift[0]
            p0 -= shift[0] / num_atoms
        p0[j] += shift[1]
        p0 -= shift[1] / num_atoms

    zsum, perms, p0 = best
    rmsd = np.sqrt(zsum / num_atoms)

    perm = []
    for p in perms:
        _, a = list(zip(*p))
        a = np.array(a)
        a += len(perm)
        perm += list(a)
    perm = np.array(perm)

    atoms0 = Atoms(numbers, positions=p0, cell=intermediate_cell,
                   pbc=[1, 1, 0])
    atoms1 = Atoms(numbers[perm], positions=p1[perm], cell=intermediate_cell,
                   pbc=[1, 1, 0])
    return rmsd, atoms0, atoms1


def parallelogram_area(a, b):
    return np.linalg.norm(np.cross(a, b))


def adjust_z_scale(pos, cell, intermediate_cell, imarea):
    area = parallelogram_area(cell[0], cell[1])
    k = (np.sqrt(imarea / area) *
         cell[2, 2] / intermediate_cell[2, 2])

    adjusted_pos = np.copy(pos)
    adjusted_pos[:, 2] *= k
    return adjusted_pos


def project_into_intermediate_cell(cell0, cell1, pos0, pos1):

    # decompose linear map into orthogonal and symmetric matrices
    U, P = polar(np.linalg.solve(cell0, cell1), side='right')
    M = (np.identity(len(P)) + P) / 2
    intermediate_cell = np.dot(cell0, M)

    imarea = parallelogram_area(intermediate_cell[0], intermediate_cell[1])
    adjusted_pos0 = adjust_z_scale(pos0, cell0, intermediate_cell, imarea)
    adjusted_pos1 = adjust_z_scale(pos1, cell1, intermediate_cell, imarea)
    return intermediate_cell, adjusted_pos0, adjusted_pos1


def get_atom_data(atoms):
    pos = atoms.get_scaled_positions(wrap=True)
    cell = np.copy(atoms.cell)
    numbers = np.copy(atoms.numbers)

    # verify that unit cell is right-handed matrix
    assert np.sign(np.linalg.det(cell)) == 1

    # verify that unit cell is for a 2D material
    for i, j in [(0, 2), (1, 2), (2, 0), (2, 1)]:
        if abs(cell[i, j]) >= 1e-12:
            warnings.warn('Bad cell: {}'.format(cell))
        cell[i, j] = 0
    return pos, cell, numbers


def calculate_rmsd(atoms0, atoms1, zscale=False):
    """Calculates the optimal RMSD between two monolayer structures.

    A common frame of reference is calculated and the optimal translation is
    found.  The unit cells of the returned atoms objects define the common
    frame of reference.  At the moment, the input structures must have the
    exact same stoichiometry.

    atoms0: The first (monolayer) atoms object.
    atoms1: The second (monolayer) atoms object.

    Returns
    =======

    rmsd: float
        The RMSD value.
    opt0: An atoms object
        The positions are the optimally translated
        positions from atoms0.
    opt1: An atoms object
        The positions are the optimally translated
        positions from atoms1.

    """

    # atoms should be niggli reduced prior to calling this function

    pos0, cell0, numbers0 = get_atom_data(atoms0)
    pos1, cell1, numbers1 = get_atom_data(atoms1)

    # only allow identical stoichimetries for now
    assert(sorted(numbers0) == sorted(numbers1))

    intermediate_cell, adjusted_pos0, adjusted_pos1 = \
        project_into_intermediate_cell(cell0, cell1, pos0, pos1)
    return best_alignment(intermediate_cell,
                          adjusted_pos0, adjusted_pos1,
                          numbers0, numbers1,
                          zscale=zscale)


def calculate_unit_cell_difference(atoms0, atoms1):
    """Calculates the geometrical difference between unit cells (modulo
    rotation), as a scalar value.  The difference is calculated as the
    Frobenius norm of the difference between the strain matrix and the
    identity matrix.  The strain matrix is obtained from a right-sided
    polar decomposition of the linear map which maps the first cell onto
    the second.

    atoms0: The first atoms object.
    atoms1: The second atoms object.

    Returns
    =======

    change: float
        The scalar value of the difference between the unit cells.
    """

    # calculate linear map (ignore out-of-plane direction)
    M = np.linalg.solve(atoms0.cell[:2, :2], atoms1.cell[:2, :2])

    # decompose into orthogonal and symmetric (strain) matrices
    U, P = polar(M, side='right')
    assert(np.linalg.norm(P - P.T) < 1E-14)

    # Frobenius norm of difference from identity matrix
    return np.linalg.norm(P - np.identity(len(P)))
