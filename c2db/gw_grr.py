# Creates: gs_gw_grr_nowfs.gpw

from gpaw import GPAW
from c2db.utils import get_kpts_size
from c2db.gw import get_bandrange
# from c2db.gw import gw_results
import os.path as op


def gw_calc(kptdensity=5):
    """Calculate the gw bandstructure, using "fake" gs_gw.gpw if it is not
    there"""

    for name in ['gs.gpw']:
        if not op.isfile(name):
            return
    calc = GPAW('gs.gpw', txt=None)

    lb, ub = get_bandrange(calc)
    gpw = 'gs_gw_grr_nowfs.gpw'
    if not op.isfile(gpw):
        kpts = get_kpts_size(atoms=calc.atoms, density=kptdensity)
        calc.set(kpts=kpts,
                 fixdensity=True,
                 nbands=ub + 12,
                 convergence={'bands': ub + 2},
                 txt='gs_gw_grr.txt')
        calc.get_potential_energy()
        calc.get_forces()
        calc.get_stress()
        calc.write(gpw)

    # gw_results(bandrange=list(range(lb, ub)), name='gw_grr')


if __name__ == '__main__':
    gw_calc()
