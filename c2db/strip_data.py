"""Strip almost all data."""

from ase.db import connect


def strip(db1, db2):
    for row in db1.select():
        data = row.data
        newdata = {}

        for key in ['chdata',
                    'references',
                    'effectivemass',
                    'phonon_frequencies_2d',
                    'phonon_frequencies_3d',
                    'phonon_energies_2d',
                    'phonon_energies_3d',
                    'deformation_potentials',
                    'deformation_potentials_nosoc']:
            if key in data:
                newdata[key] = data[key]

        if 'bs_pbe' in data:
            newdata['bs_pbe'] = {}
            for key in ['kvbm', 'kcbm', 'kvbm_nosoc', 'kcbm_nosoc']:
                newdata['bs_pbe'][key] = data.bs_pbe.get(key)

        kvp = row.key_value_pairs
        a = row.toatoms()
        db2.write(a, data=newdata, **kvp)
        print('.', end='', flush=True)

    print()


if __name__ == '__main__':
    import sys
    db1 = connect(sys.argv[1])
    with connect(sys.argv[2]) as db2:
        strip(db1, db2)
