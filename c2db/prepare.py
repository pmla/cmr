import argparse
import json
import os
from collections import Counter

from ase import Atoms
from ase.build import mx2
from ase.io import read
from ase.data import covalent_radii, atomic_numbers
from ase.parallel import world
from ase.spacegroup import crystal

from c2db.rmsd_monolayer import calculate_rmsd


def bii3(slab):
    symbols = slab.get_chemical_symbols()
    N = Counter(symbols)
    (B, nb), (A, na) = N.most_common()
    assert nb == 6 and na == 2, N
    a = slab.cell[0, 0]
    t = slab.positions[:, 2].ptp() / 2
    for s, R in zip(symbols, slab.positions):
        if s == A:
            dists = ((slab.positions - R)**2).sum(axis=1)**0.5
            break
    d = sorted(dists)[1]
    u = (d**2 - t**2 - a**2 * 3 / 6**2)**0.5

    L = 10
    c = crystal([A, B],
                spacegroup=162,
                cellpar=[a, a, L, 90, 90, 120],
                basis=[(2 / 3, 1 / 3, 0.5), (0.5 - u / a, 0, 0.5 + t / L)],
                pbc=(1, 1, 0))
    return c


def ticl3(slab):
    symbols = slab.get_chemical_symbols()
    N = Counter(symbols)
    (B, nb), (A, na) = N.most_common()
    assert nb == 6 and na == 2, N
    a = slab.cell[0, 0]
    t = slab.positions[:, 2].ptp() / 2
    for s, R in zip(symbols, slab.positions):
        if s == A:
            dists = ((slab.positions - R)**2).sum(axis=1)**0.5
            break
    d = sorted(dists)[1]
    u = (d**2 - t**2 - a**2 * 3 / 6**2)**0.5

    L = 10
    c = crystal([A, B],
                spacegroup=189,
                cellpar=[a, a, L, 90, 90, 120],
                basis=[(2 / 3, 1 / 3, 0.5), (0.5 - u / a, 0, 0.5 + t / L)],
                pbc=(1, 1, 0))
    return c


def convert_to_canonical_structure(atoms, prototype):
    if prototype in ['MoS2', 'CdI2']:
        assert len(atoms) == 3
        M, X, Y = atoms.get_chemical_symbols()
        assert X == Y
        a, b, c, alpha, beta, gamma = atoms.get_cell_lengths_and_angles()
        thickness = atoms.positions[:, 2].ptp()
        kind = {'MoS2': '2H', 'CdI2': '1T'}[prototype]
        atoms = mx2(M + X + X, kind, a, thickness)
    elif prototype == 'Ga2S2':
        assert len(atoms) == 4
        A1, A2, B1, B2 = atoms.get_chemical_symbols()
        assert A1 == A2 and B1 == B2
        z = atoms.positions[:, 2]
        assert z[2] < z[0] < z[1] < z[3]
        daa = z[1] - z[0]
        dab = z[3] - z[1]
        a = (atoms.cell[0]**2).sum()**0.5
        atoms = Atoms(A1 * 2 + B1 * 2,
                      cell=[[a, 0, 0],
                            [-a / 2, a * 3**0.5 / 2, 0],
                            [0, 0, 1]],
                      pbc=[1, 1, 0],
                      scaled_positions=[[0, 0, -daa / 2],
                                        [0, 0, daa / 2],
                                        [2 / 3, 1 / 3, -daa / 2 - dab],
                                        [2 / 3, 1 / 3, daa / 2 + dab]])
    elif prototype in ['BN']:
        assert len(atoms) == 2
        A, B = atoms.get_chemical_symbols()
        a = (atoms.cell[0]**2).sum()**0.5
        b = (atoms.cell[1]**2).sum()**0.5
        assert a - b < 1e-6
        atoms = Atoms(A + B,
                      cell=[[a, 0, 0],
                            [-a / 2, a * 3**0.5 / 2, 0],
                            [0, 0, 1]],
                      pbc=[1, 1, 0],
                      positions=[[0, a / 3**0.5, 0],
                                 [a / 2, a / 3**0.5 / 2, 0]])
    elif prototype in ['FeSe']:
        assert len(atoms) == 4
        A1, A2, B1, B2 = atoms.get_chemical_symbols()
        assert A1 == A2 and B1 == B2
        z = abs(sum(abs(atoms.positions[1:2, 2])) -
                sum(abs(atoms.positions[2:3, 2])))
        a = (atoms.cell[0]**2).sum()**0.5
        b = (atoms.cell[1]**2).sum()**0.5
        assert a - b < 1e-6
        atoms = Atoms(A1 * 2 + B1 * 2,
                      cell=[[a, 0, 0],
                            [0, a, 0],
                            [0, 0, 1]],
                      pbc=[1, 1, 0],
                      positions=[[0, 0, 0],
                                 [a / 2, a / 2, 0],
                                 [0, a / 2, -z],
                                 [a / 2, 0, z]])
    elif prototype in ['C3N']:
        assert len(atoms) == 8
        A1, B1, A2, A3, A4, A5, B2, A6 = atoms.get_chemical_symbols()
        assert A1 == A2 == A3 == A4 == A5 == A6
        assert B1 == B2
        a = abs(atoms.positions[7, 0] - atoms.positions[0, 0]) / 4
        atoms = Atoms(A1 + B1 + A1 * 4 + B1 + A1,
                      cell=[[a * 3, +a * 3**0.5, 0],
                            [a * 3, -a * 3**0.5, 0],
                            [0, 0, 1]],
                      pbc=[1, 1, 0],
                      positions=[[0, 0, 0],
                                 [a, 0, 0],
                                 [a * 1.5, +a * 3**0.5 / 2, 0],
                                 [a * 1.5, -a * 3**0.5 / 2, 0],
                                 [a * 2.5, +a * 3**0.5 / 2, 0],
                                 [a * 2.5, -a * 3**0.5 / 2, 0],
                                 [a * 3, 0, 0],
                                 [a * 4, 0, 0]])
    elif prototype in ['TiS3']:
        assert len(atoms) == 8
        A1, A2, B1, B2, B3, B4, B5, B6 = atoms.get_chemical_symbols()
        assert A1 == A2 and B1 == B2 == B3 == B4 == B5 == B6
        # rectengular cell, no specific ratio
        a = atoms.cell[0, 0]
        b = atoms.cell[1, 1]
        # ref to center
        c = 0.5 * (atoms.positions[1, :] + atoms.positions[0, :])

        # zA - A in z
        zA = abs(atoms.positions[0, 2] - c[2])
        # ratio in a: ra and (0.5-ra)
        ra = abs(atoms.positions[4, 0] - c[0]) / a
        # zB1/2, B in z - different, but no relation
        zB1 = abs(atoms.positions[2, 2] - c[2])
        zB2 = abs(atoms.positions[4, 2] - c[2])

        # depends on (a,b,ra,zA,zB1,zB2)
        atoms = Atoms(A1 * 2 + B1 * 6,
                      cell=[[a, 0, 0],
                            [0, b, 0],
                            [0, 0, 10.]],
                      pbc=[1, 1, 0],
                      positions=[[a / 4, -b / 4, zA],
                                 [-a / 4, b / 4, -zA],
                                 [a / 4, b / 4, -zB1],
                                 [-a / 4, -b / 4, zB1],
                                 [ra * a, b / 4, zB2],
                                 [-ra * a, -b / 4, -zB2],
                                 [(0.5 - ra) * a, b / 4, zB2],
                                 [-(0.5 - ra) * a, -b / 4, -zB2]])
    elif prototype in ['GeS']:
        pass
    elif prototype in ['GaS']:
        pass
    elif prototype in ['A2PbI4']:
        pass
    elif prototype == 'BiI3':
        atoms = bii3(atoms)
    elif prototype == 'TiCl3':
        atoms = ticl3(atoms)
    elif prototype in ['Ti2CO2']:
        pass
    elif prototype in ['Ti3C2O2']:
        pass
    elif prototype in ['Ti4C3O2']:
        pass
    elif prototype in ['P4']:
        pass
    elif prototype in ['Pb2Se2']:
        pass
    elif prototype in ['Hg3As2']:
        # first reported in  Chin.Phys.Lett. 34 5 (2017) 057302
        pass
    elif prototype in ['FeOCl']:
        assert len(atoms) == 6
        A1, A2, B1, B2, C1, C2 = atoms.get_chemical_symbols()
        assert A1 == A2 and B1 == B2 and C1 == C2
        # rectengular cell, no specific ratio
        a = atoms.cell[0, 0]
        b = atoms.cell[1, 1]
        c0 = sum(atoms.positions[:, 2]) / len(atoms)

        # A, B, C in z
        zA = abs(atoms.positions[0, 2] - c0)
        zB = abs(atoms.positions[2, 2] - c0)
        zC = abs(atoms.positions[4, 2] - c0)

        # depends on (a,b,zA,zB,zC)
        atoms = Atoms(A1 * 2 + B1 * 2 + C1 * 2,
                      cell=[[a, 0, 0],
                            [0, b, 0],
                            [0, 0, 10.]],
                      pbc=[1, 1, 0],
                      positions=[[a / 2, 0, -zA],
                                 [0, b / 2, zA],
                                 [0, 0, -zB],
                                 [a / 2, b / 2, zB],
                                 [0, 0, zC],
                                 [a / 2, b / 2, -zC]])
    else:
        import warnings
        warnings.warn('Please consider using a known prototype or '
                      'implementing a new prototype')
    return atoms


def find_proto(atoms, verbose=False):
    best = 'none'
    rmsd = 1e6
    for p in ['MoS2', 'CdI2', 'Ga2S2', 'BN', 'FeSe', 'C3N', 'TiS3', 'FeOCl']:
        for rep in [(1, 1, 1), (2, 1, 1), (1, 2, 1), (2, 2, 1)]:
            try:
                a = convert_to_canonical_structure(atoms.repeat(rep), p)
                r = calculate_rmsd(atoms, a)
            except Exception:
                pass
            else:
                if verbose:
                    print(p, rep, r[0])
                if r[0] < rmsd:
                    rmsd = r[0]
                    best = p
    return best


def extendProto(slab):
    """Extend prototype to collection"""
    gr = []
    # main groups
    gr.append(['Li', 'Na', 'K', 'Rb', 'Cs'])
    gr.append(['Be', 'Mg', 'Ca', 'Sr', 'Ba'])
    gr.append(['B', 'Al', 'Ga', 'In', 'Tl'])
    gr.append(['C', 'Si', 'Ge', 'Sn', 'Pb'])
    gr.append(['N', 'P', 'As', 'Sb', 'Bi'])
    gr.append(['O', 'S', 'Se', 'Te'])
    gr.append(['Cl', 'Br', 'I'])
    # noble
    gr.append(['Cu', 'Ag', 'Au'])
    # tm
    gr.append(['Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe',
               'Co', 'Ni', 'Zr', 'Nb', 'Mo', 'Ru',
               'Rh', 'Pb', 'Hf', 'Ta', 'W', 'Re',
               'Os', 'Ir', 'Pt'])

    # get element list
    elli = list(set(slab.get_chemical_symbols()))
    # replace lists
    reli = []
    for i in elli:
        for g in gr:
            if i in g:
                reli.append(g)

    # find all possible permutations
    slabs = []
    # simple case of two elements, permutating both
    if len(reli) == 2:
        import itertools
        import copy
        fac = (covalent_radii[atomic_numbers[elli[0]]] +
               covalent_radii[atomic_numbers[elli[1]]])
        for p in itertools.product(reli[0], reli[1]):
            new = copy.deepcopy(slab)
            # replace elements
            elems = slab.get_chemical_symbols()
            elems = [p[0] if x == elli[0] else x for x in elems]
            elems = [p[1] if x == elli[1] else x for x in elems]
            new.set_chemical_symbols(elems)
            # approximated scaling
            fac2 = (covalent_radii[atomic_numbers[p[0]]] +
                    covalent_radii[atomic_numbers[p[1]]])
            new.set_cell(new.get_cell() * fac2 / fac, scale_atoms=True)
            slabs.append(new)
    else:
        raise NotImplementedError("No permutation for > 2 types!")

    return slabs


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--prototype', help='One of MoS2, ..., auto',
                        default='none')
    parser.add_argument('-c', '--create-folder', action='store_true',
                        help='Create a "Formula-Prototype/" folder.')
    parser.add_argument('-C', '--create-collection', action='store_true')
    parser.add_argument('-f', '--find-prototype', action='store_true')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('--db', action='store_true')
    parser.add_argument('filename', help='Filename of input structure.')
    args = parser.parse_args()

    # go through db
    if args.db:
        from ase.db import connect
        db = connect(args.filename)
        for row in db.select():
            atoms = row.toatoms()
            print(atoms.get_chemical_formula(mode='metal'))
            print(find_proto(atoms, verbose=args.verbose))

    # load atoms (maybe reworked)
    atoms = read(args.filename)
    prototype = args.prototype
    if prototype == 'auto':
        prototype = find_proto(atoms)
    if prototype is not 'none':
        atoms = convert_to_canonical_structure(atoms, prototype)

    # build folders, one or more
    at_list = []
    if args.create_folder:
        at_list = [atoms]
    elif args.create_collection:
        at_list = extendProto(atoms)
    for at in at_list:
        formula = at.get_chemical_formula('metal')
        if prototype is not 'none':
            folder = '{}-{}'.format(formula, prototype)
        else:
            folder = formula
        if world.rank == 0:
            os.mkdir(folder)
            at.write(os.path.join(folder, 'guess.xyz'))
            with open(os.path.join(folder, 'info.json'), 'w') as fd:
                fd.write(json.dumps({'prototype': prototype}))

    # find prototype
    if args.find_prototype:
        print('Best match: ' + find_proto(atoms, verbose=args.verbose))
