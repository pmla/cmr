from pathlib import Path

from ase.io import read, write
from gpaw import GPAW, FermiDirac, PW

from c2db.relax import relax_done, relax


def relax2x2():
    """Relax atomic positions and 1x2 cell. """
    state = Path().cwd().parts[-1]
    slab0 = read('../relax-{}.traj'.format(state))

    slab0 = slab0.repeat((2, 2, 1))

    params = dict(
        mode=PW(800),
        xc='PBE',
        kpts={'density': 6.0, 'gamma': True},
        occupations=FermiDirac(width=0.05))

    # single point
    name = '{}-2x2'.format(state)
    if not Path(name + '.traj').is_file():
        slab0.calc = GPAW(txt=name + '.txt',
                          **params)
        slab0.get_forces()
        slab0.get_stress()
        e0 = slab0.get_potential_energy()
        write(name + '.traj', slab0)
    else:
        e0 = read(name + '.traj').get_potential_energy()

    tag = '{}-2x2'.format(state)

    emin = e0 - 0.01 * len(slab0)
    slab, done = relax_done('relax-' + tag + '.traj', emin=emin)
    if not done:
        if slab is None:
            slab = slab0
            slab.rattle(stdev=0.01)
        relax(slab, tag, emin=emin)


def dyn_stab():
    from c2db.rmsd_monolayer import calculate_rmsd
    e1 = read('ref_2x2.traj').get_potential_energy()
    e2 = read('relax-spin-paired_2x2.traj').get_potential_energy()
    s2 = read('relax-spin-paired_2x2.traj')
    if read('relax-spin-polarized_2x2.traj').get_potential_energy() < e2:
        e2 = read('relax-spin-polarized_2x2.traj').get_potential_energy()
        s2 = read('relax-spin-polarized_2x2.traj')
    d2 = calculate_rmsd(read('ref_2x2.traj'), s2)[0]

    return (e2 - e1) / len(s2), d2


if __name__ == '__main__':
    relax2x2()
