import argparse
import os
import sys

from ase.io import read
from c2db import chdir
from c2db.relax import relax_done
from c2db.stability import dyn_stab
from c2db.references import read_references, formation_energy


def check_relax(verbose=False):
    try:
        read('gs.gpw')
        gs = True
    except Exception:
        if verbose:
            print("check: No gs.gpw!")
        gs = False
    return (gs and
            relax_done('relax-spin-paired.traj', verbose=verbose) and
            relax_done('relax-spin-polarized.traj', verbose=verbose))


def check_hform(verbose=False):
    try:
        atoms = read('gs.gpw')
        hform = formation_energy(atoms, read_references()) / len(atoms)
        res = hform <= 1
    except Exception:
        res = False
    return res


def check_relax2x2(verbose=False):
    try:
        read('gs_2x2.gpw')
        read('ref_2x2.traj').get_potential_energy()
        gs = True
    except Exception:
        if verbose:
            print("check: No gs_2x2.gpw or ref_2x2!")
        gs = False
    return (gs and
            relax_done('relax-spin-paired_2x2.traj', verbose=verbose) and
            relax_done('relax-spin-polarized_2x2.traj', verbose=verbose))


def check_dyn(verbose=False):
    try:
        ediff, ddiff = dyn_stab()
        res = ediff <= 10e-3 and ddiff <= 0.05
    except Exception:
        res = False
    return res


def check(verbose=False):
    if not check_relax(verbose):
        return False
    else:
        if not check_hform(verbose):
            return True
        else:
            if not check_relax2x2(verbose):
                return False
            else:
                if not check_dyn(verbose):
                    return True
                else:
                    # check more...
                    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('folder', nargs='+')
    args = parser.parse_args()

    for folder in args.folder:
        if not os.path.isdir(folder):
            continue
        with chdir(folder):
            print(folder, end=': ')
            if check(verbose=True):
                print("ok")
                # silence please...
                if not args.verbose:
                    sys.stdout.write("\033[F")
