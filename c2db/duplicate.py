# Creates: duplicate

import os
import os.path as op
import sys
from ase.db import connect
from ase.io import read
from c2db.check_duplicates import find_duplicates

folders = sys.argv[1:]
if not folders:
    folders = [os.getcwd()]

db = connect('/home/niflheim2/cmr/db-files/c2db.db')
rows = [x for x in db.select()]

for folder in folders:
    state = op.basename(op.normpath(folder))
    structure = read(op.join(folder, '../relax-{}.traj').format(state))

    is_duplicate = find_duplicates(structure, rows, use_magstate=True)
    for val in is_duplicate:
        row = db.get(int(val))
        parent_folder = op.join(op.abspath(folder), '../')
        parent_comparison = op.join(op.abspath(row.folder), '../')
        if op.normpath(parent_folder) != op.normpath(parent_comparison):
            with open('duplicate', 'w') as f:
                f.write("{} is a duplicate of "
                        "{}!\n".format(folder, row.folder))
            raise ValueError("Duplicate structure found. Stopping workflow!")
