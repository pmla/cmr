"""Check that all figures can be generated."""
import os
import sys
from pathlib import Path
from ase.db import connect
from ase.db.summary import Summary
from ase.db.web import process_metadata


filename = sys.argv[1]
if len(sys.argv) == 4:
    n, N = (int(x) for x in sys.argv[2:])
else:
    n = 0
    N = 1

tmp = 'tmp{}'.format(n)
p = Path() / tmp
if not p.is_dir():
    p.mkdir()


db = connect(filename)
db.python = 'custom.py'
db.meta = process_metadata(db)

os.chdir(str(p))
for row in db.select():
    if row.id % N == n:
        print(row.id, row.formula, row.prototype)
        Summary(row, db.meta,
                prefix='c2db-{}-'.format(row.id),
                tmpdir='.').write()
