import numpy as np
from numpy import linalg as la

from scipy.interpolate import CubicSpline

from ase.dft.kpoints import bandpath, get_monkhorst_pack_size_and_offset,\
    monkhorst_pack_interpolate

from c2db.utils import get_special_2d_path, eigenvalues


def _lti(calc, kpts_kc, e_skn=None):
    """
    linear tetrahedron interpolation
    Parameters:
        calc: Calculator
            GPAW calcualtor
        kpts_kc: (nk, 3)-shape array
            kpoints to interpolate onto
        e_skn: (ns, nk, nb)-shape array (optional)
            array values on the kpoint grid in calc
            if not specified it takes the eigenvalues from calc
    Returns:
        eps_skn: (ns ,nk, nb)-shape array
            the array values in e_skn interpolated onto kpts_kc
    """
    if e_skn is None:
        e_skn = eigenvalues(calc)
    atoms = calc.get_atoms()
    icell = atoms.get_reciprocal_cell()
    bz2ibz = calc.get_bz_to_ibz_map()
    size, offset = get_monkhorst_pack_size_and_offset(calc.get_bz_k_points())
    eps = monkhorst_pack_interpolate(kpts_kc, e_skn.transpose(1, 0, 2),
                                     icell, bz2ibz, size, offset)
    return eps.transpose(1, 0, 2)


def interpol(calc, eps_skn):
    """stuff from calc file and eps_skn
    """
    pass
    # from ase.dft.kpoints import parse_path_string
    # from ase.dft.kpoints import get_special_points
    # from gpaw.kpt_descriptor import to1bz

    # atoms = calc.get_atoms()
    # cell = atoms.get_cell()
    # pathstr = get_special_2d_path(cell=cell)
    # sp = get_special_points(cell)
    # path = parse_path_string(pathstr)[0]
    # kpts_kc = np.array([sp[k] for k in path])

    # bz2ibz = calc.get.get_bz_to_ibz_map()
    # kptsbz_kc = calc.get_bz_k_points()
    # kpts1bz_kc = to1bz(bzkpts_kc)


def ontheline(p1, p2, p3s, eps=1.0e-5):
    """
    line = p1 + t * (p2 - p1)
    check whether p3 is on the line (t is between 0 and 1)
    Parameters:
        p1, p2: ndarray
            point defining line p1, p2 and third point p3 we are checking
        p3s: list [ndarray,]
        eps: float
            slack in distance to be considered on the line
    Returns:
        indices: [(int, float), ] * Np
            indices and t's for p3s on the line,
            i.e [(0, 0.1), (4, 0.2), (3, 1.0], sorted accorting to t
    """
    nk = len(p3s)
    kpts = np.zeros((nk * 4, 3))
    kpts[:nk] = p3s
    kpts[nk:2 * nk] = p3s - (1, 0, 0)
    kpts[2 * nk:3 * nk] = p3s - (1, 1, 0)
    kpts[3 * nk:4 * nk] = p3s - (0, 1, 0)
    d = p2 - p1  # direction
    d2 = np.dot(d, d)
    its = []
    for i, p3 in enumerate(kpts):
        t = np.dot(d, p3 - p1) / d2
        x = p1 + t * d  # point on the line that minizes distance to p3
        dist = la.norm(x - p3)
        if (0 - eps <= t <= 1 + eps) and (dist < eps):
            its.append((i % nk, t))
    its = sorted(its, key=lambda x: x[1])
    return its


def segment_indices_and_x(cell, path_str, kpts):
    """finds indices of bz k-points that is located on segments of a bandpath
    Parameters:
        cell: ndarray (3, 3)-shape
            unit cell
        path_str: str
            i.e. 'GMKG'
        kpts: ndarray (nk, 3)-shape
    Returns:
        out: ([[int,] * Np,] * Ns, [[float,] * Np, ] * Ns)
            list of indices and list of x
    """
    from ase.dft.kpoints import parse_path_string, get_special_points
    special = get_special_points(cell)
    _, _, X = bandpath(path=path_str, cell=cell, npoints=len(path_str))
    segments_length = np.diff(X)  # length of band segments
    path = parse_path_string(path_str)[0]  # list str, i.e. ['G', 'M', 'K','G']
    segments_points = []
    # make segments [G,M,K,G] -> [(G,M), (M,K), (K.G)]
    for i in range(len(path) - 1):
        kstr1, kstr2 = path[i:i + 2]
        s1, s2 = special[kstr1], special[kstr2]
        segments_points.append((s1, s2))

    # find indices where kpts is on the segments
    segments_indices = []
    segments_xs = []
    for (k1, k2), d, x0 in zip(segments_points, segments_length, X):
        its = ontheline(k1, k2, kpts)
        indices = [i for i, t in its]
        ts = np.asarray([t for i, t in its])
        xs = ts * d  # positions on the line of length d
        segments_xs.append(xs + x0)
        segments_indices.append(indices)

    return segments_indices, segments_xs


def interpolate_bandstructure(calc, e_skn=None, npoints=400):
    """simple wrapper for interpolate_bandlines2
    Returns:
        out: kpts, e_skn, xreal, epsreal_skn
    """
    path = get_special_2d_path(cell=calc.atoms.cell)
    r = interpolate_bandlines2(calc=calc, path=path, e_skn=e_skn,
                               npoints=npoints)
    return r['kpts'], r['e_skn'], r['xreal'], r['epsreal_skn']


def interpolate_bandlines2(calc, path, e_skn=None, npoints=400):
    """Interpolate bandstructure
    Parameters:
        calc: ASE calculator
        path: str
            something like GMKG
        e_skn: (ns, nk, nb) shape ndarray, optional
            if not given it uses eigenvalues from calc
        npoints: int
            numper of point on the path
    Returns:
        out: dict
            with keys e_skn, kpts, x, X
            e_skn: (ns, npoints, nb) shape ndarray
                interpolated eigenvalues,
            kpts:  (npoints, 3) shape ndarray
                kpts on path (in basis of reciprocal vectors)
            x: (npoints, ) shape ndarray
                x axis
            X: (nkspecial, ) shape ndarrary
                position of special points (G, M, K, G) on x axis

    """
    if e_skn is None:
        e_skn = eigenvalues(calc)
    kpts = calc.get_bz_k_points()
    bz2ibz = calc.get_bz_to_ibz_map()
    cell = calc.atoms.cell
    indices, x = segment_indices_and_x(cell=cell, path_str=path, kpts=kpts)
    # kpoints and positions to interpolate onto
    kpts2, x2, X2 = bandpath(cell=cell, path=path, npoints=npoints)
    # remove double points
    for n in range(len(indices) - 1):
        if indices[n][-1] == indices[n + 1][0]:
            del indices[n][-1]
            x[n] = x[n][:-1]
    # flatten lists [[0, 1], [2, 3, 4]] -> [0, 1, 2, 3, 4]
    indices = [a for b in indices for a in b]
    kptsreal_kc = kpts[indices]
    x = [a for b in x for a in b]
    # loop over spin and bands and interpolate
    ns, nk, nb = e_skn.shape
    e2_skn = np.zeros((ns, len(x2), nb), float)
    epsreal_skn = np.zeros((ns, len(x), nb), float)
    for s in range(ns):
        for n in range(nb):
            e_k = e_skn[s, :, n]
            y = [e_k[bz2ibz[i]] for i in indices]
            epsreal_skn[s, :, n] = y
            bc_type = ['not-a-knot', 'not-a-knot']
            for i in [0, -1]:
                if path[i] == 'G':
                    bc_type[i] = [1, 0.0]
            sp = CubicSpline(x=x, y=y, bc_type=bc_type)
            # sp = InterpolatedUnivariateSpline(x, y)
            e2_skn[s, :, n] = sp(x2)
    results = {'kpts': kpts2,    # kpts_kc on bandpath
               'e_skn': e2_skn,  # eigenvalues on bandpath
               'x': x2,          # distance along bandpath
               'X': X2,          # positons of vertices on bandpath
               'xreal': x,       # distance along path (at MonkhorstPack kpts)
               'epsreal_skn': epsreal_skn,  # path eigenvalues at MP kpts
               'kptsreal_kc': kptsreal_kc   # path k-points at MP kpts
               }
    return results
