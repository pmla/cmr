"""Fix broken things."""
import json
import sys
from pathlib import Path


def fix_gllbsc():
    """Fix bad json files."""
    for path in Path().glob('**/gllbsc.json'):
        print(path, end=' ')
        text = path.read_text()
        dct = json.loads(text)
        write = False
        for key in ['vbm_gllbsc', 'cbm_gllbsc',
                    'vbm_gllbsc_nosoc', 'cbm_gllbsc_nosoc']:
            if isinstance(dct.get(key), list):
                del dct[key]
                write = True
        if write:
            backup = path.with_name('gllbsc-broken.json')
            if not backup.is_file():
                backup.write_text(text)
            path.write_text(json.dumps(dct))
            print('fixed')
        else:
            print()


def fix_c2dm():
    """Rename files."""
    for path in Path().glob('**/.tasks'):
        done = {}
        for line in path.read_text().splitlines():
            date, state, name, *_ = line.split()
            name = name.replace('c2dm', 'c2db')
            done[name] = (state == 'done')
        for task, finished in done.items():
            if finished:
                d = path.with_name(task + '.done')
                d.write_text('')
                print(d)
        path.rename(path.with_name('tasks.old'))

    for path in Path().glob('**/c2dm.*.done'):
        path.rename(path.with_name(path.name.replace('c2dm', 'c2db')))
        print(path)


if __name__ == '__main__':
    fix = sys.argv[1]
    if fix == 'gllbsc':
        fix_gllbsc()
    elif fix == 'c2dm':
        fix_c2dm()
