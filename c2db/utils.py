import numpy as np


from ase.geometry import crystal_structure_from_cell as csfc
from ase.units import Ha
from ase.dft.kpoints import parse_path_string, get_special_points, bandpath
from ase.dft.kpoints import kpoint_convert
from ase.parallel import broadcast
from gpaw import mpi
from gpaw.occupations import occupation_numbers
from gpaw.symmetry import atoms2symmetry
from gpaw.kpt_descriptor import kpts2sizeandoffsets as k2so

norm = np.linalg.norm


def fermi_level(calc, eps_skn=None, nelectrons=None):
    """
    Parameters:
        calc: GPAW
            GPAW calculator
        eps_skn: ndarray, shape=(ns, nk, nb), optional
            eigenvalues (taken from calc if None)
        nelectrons: float, optional
            number of electrons (taken from calc if None)
    Returns:
        out: float
            fermi level
    """
    if nelectrons is None:
        nelectrons = calc.get_number_of_electrons()
    if eps_skn is None:
        eps_skn = eigenvalues(calc)
    eps_skn.sort(axis=-1)
    occ = calc.occupations.todict()
    weight_k = calc.get_k_point_weights()
    return occupation_numbers(occ, eps_skn, weight_k, nelectrons)[1] * Ha


def eigenvalues(calc):
    """
    Parameters:
        calc: Calculator
            GPAW calculator
    Returns:
        e_skn: (ns, nk, nb)-shape array
    """
    rs = range(calc.get_number_of_spins())
    rk = range(len(calc.get_ibz_k_points()))
    e = calc.get_eigenvalues
    return np.asarray([[e(spin=s, kpt=k) for k in rk] for s in rs])


def gpw2eigs(gpw, soc=True, bands=None, return_spin=False,
             optimal_spin_direction=False):
    """give the eigenvalues w or w/o spinorbit coupling and the corresponding
    fermi energy
    Parameters:
        gpw: str
            gpw filename
        soc: None, bool
            use spinorbit coupling if None it returns both w and w/o
        optimal_spin_direction: bool
            If True, use get_spin_direction to calculate the spin direction
            for the SOC
        bands: slice, list of ints or None
            None gives parameters.convergence.bands if possible else all bands
        Returns: dict or e_skn, efermi
        containg eigenvalues and fermi levels w and w/o spinorbit coupling
    """
    from gpaw import GPAW
    from gpaw.spinorbit import get_spinorbit_eigenvalues
    ranks = [0]
    comm = mpi.world.new_communicator(ranks)
    dct = None
    if mpi.world.rank in ranks:
        theta = 0
        phi = 0
        if optimal_spin_direction:
            theta, phi = get_spin_direction()
        calc = GPAW(gpw, txt=None, communicator=comm)
        if bands is None:
            n2 = calc.todict().get('convergence', {}).get('bands')
            bands = slice(0, n2)
        if isinstance(bands, slice):
            bands = range(calc.get_number_of_bands())[bands]
        eps_nosoc_skn = eigenvalues(calc)[..., bands]
        efermi_nosoc = calc.get_fermi_level()
        eps_mk, s_kvm = get_spinorbit_eigenvalues(calc, bands=bands,
                                                  theta=theta,
                                                  phi=phi,
                                                  return_spin=True)
        eps_km = eps_mk.T
        efermi = fermi_level(calc, eps_km[np.newaxis],
                             nelectrons=2 *
                             calc.get_number_of_electrons())
        dct = {'eps_nosoc_skn': eps_nosoc_skn,
               'eps_km': eps_km,
               'efermi_nosoc': efermi_nosoc,
               'efermi': efermi,
               's_kvm': s_kvm}

    dct = broadcast(dct, root=0, comm=mpi.world)
    if soc is None:
        return dct
    elif soc:
        out = (dct['eps_km'], dct['efermi'], dct['s_kvm'])
        if not return_spin:
            out = out[:2]
        return out
    else:
        return dct['eps_nosoc_skn'], dct['efermi_nosoc']


def get_kpts_size(atoms, density):
    """trying to get a reasonable monkhorst size which hits high
    symmetry points (for hexagonal cells at least)
    """
    size, offset = k2so(atoms=atoms, density=density)
    size[2] = 1
    for i in range(2):
        if size[i] % 6 != 0:
            size[i] = 6 * (size[i] // 6 + 1)
    kpts = {'size': size, 'gamma': True}
    return kpts


def has_inversion(atoms, use_spglib=True):
    """
    Parameters:
        atoms: Atoms object
            atoms
        use_spglib: bool
            use spglib
    Returns:
        out: bool
    """
    try:
        import spglib
    except ImportError as x:
        import warnings
        warnings.warn('using gpaw symmetry for inversion instead: {}'
                      .format(x))
        use_spglib = False

    atoms2 = atoms.copy()
    atoms2.pbc[:] = True
    atoms2.center(axis=2)
    if use_spglib:
        R = -np.identity(3, dtype=int)
        r_n = spglib.get_symmetry(atoms2, symprec=1.0e-3)['rotations']
        return np.any([np.all(r == R) for r in r_n])
    else:
        return atoms2symmetry(atoms2).has_inversion


def get_special_2d_path(cell):
    special_2d_paths = {'hexagonal': 'GMKG',
                        'orthorhombic': 'GXSYGS',
                        'tetragonal': 'MGXM',
                        'monoclinic': 'GYHCH1XG',
                        # ...
                        }
    return special_2d_paths[csfc(cell)]


def get_bandpath_kpts(cell, npoints=400):
    path = get_special_2d_path(cell)
    kpts, _, _ = bandpath(path, cell, npoints=npoints)
    return kpts


def ibz2center(calc):
    atoms = calc.get_atoms()
    cell_cv = atoms.get_cell()
    bz2ibz = calc.get_bz_to_ibz_map()
    bz_kc = calc.get_bz_k_points()
    # find center
    path_str = get_special_2d_path(cell_cv)
    path_str_K = parse_path_string(path_str)[0]
    if path_str_K[0] == path_str_K[-1]:
        del path_str_K[-1]
    points_xc = get_special_points(cell_cv)
    points_kv = np.array([kpoint_convert(cell_cv, skpts_kc=points_xc[p])
                          for p in path_str_K])
    center_v = points_kv.mean(axis=0)
    center_c = kpoint_convert(cell_cv=cell_cv, ckpts_kv=center_v)
    # get the new ibz-kpoints centered around the center
    kbz_k, t_kc = get_kbz_and_t(center_c=center_c,
                                bz2ibz=bz2ibz,
                                bz_kc=bz_kc,
                                cell_cv=cell_cv)
    new_ibz_kc = bz_kc[kbz_k] + t_kc
    for k_c in new_ibz_kc:
        k_c[:] = np.where(abs(k_c) > 0.5, 0.0, k_c)
    return new_ibz_kc


def get_kbz_and_t(center_c, bz2ibz, bz_kc, cell_cv):
    """find irreducible k-points close to a center

    Parameters:
        center_c: float (3, )-shape ndarray
            the center in units of recirprocal lattice vectors
        bz2ibz: int (nbz, )-shape ndarray
            map between bz and ibz k-point index
        bz_kc: float (nbz, 3)-shape ndarray
        cell_cv: float (3, 3)-shape ndarray
            real space unit cell vectors

    Output:
        kbz_k, t_kc: (int (nibz, )-shape array, int (nibz, 3) ndarray)
            indices of bz kpoints and translation vectors to create a new
            set of ibz kpoints:
                new_ibzkpts_kc = (bzkpts_kc[bkz_k] + t_kc + 0.5) % 1 - 0.5
    """
    kbz_k = []
    t_kc = []
    for k in range(len(set(bz2ibz))):  # loop over ibz kpoints
        k2s = np.where(bz2ibz == k)[0]
        distmin = np.inf
        kbz = np.inf
        translation_c = (np.inf, np.inf, np.inf)
        for i1, j1 in np.ndindex(3, 3):  # loop over reciprocal lattice vectors
            i, j = i1 - 1, j1 - 1
            for k2 in k2s:
                bz_c = bz_kc[k2] + (i, j, 0)
                d_c = bz_c - center_c
                distance = norm(kpoint_convert(cell_cv, skpts_kc=d_c))
                if distance < distmin:
                    distmin = distance
                    kbz = k2
                    translation_c = (i, j, 0)
        kbz_k.append(kbz)
        t_kc.append(translation_c)
    return np.array(kbz_k), np.array(t_kc)


def get_reduced_formula(formula, stoichiometry=False):
    """
    Returns the reduced formula corresponding to a chemical formula,
    in the same order as the original formula
    E.g. Cu2S4 -> CuS2

    Parameters:
        formula (str)
        stoichiometry (bool): if True, return the stoichiometry ignoring the
          elements appearing in the formula, so for example "AB2" rather than
          "MoS2"
    Returns:
        A string containing the reduced formula
    """
    from functools import reduce
    from fractions import gcd
    import string
    import re
    split = re.findall('[A-Z][^A-Z]*', formula)
    matches = [re.match('([^0-9]*)([0-9]+)', x)
               for x in split]
    numbers = [int(x.group(2)) if x else 1 for x in matches]
    symbols = [matches[i].group(1) if matches[i] else split[i]
               for i in range(len(matches))]
    divisor = reduce(gcd, numbers)
    result = ''
    numbers = [x // divisor for x in numbers]
    numbers = [str(x) if x != 1 else '' for x in numbers]
    if stoichiometry:
        numbers = sorted(numbers)
        symbols = string.ascii_uppercase
    for symbol, number in zip(symbols, numbers):
        result += symbol + number
    return result


def get_spin_direction(fname='anisotropy_xy.npz'):
    '''
    Uses the magnetic anisotropy to calculate the preferred spin orientation
    for magnetic (FM/AFM) systems.

    Parameters:
        fname:
            The filename of a datafile containing the xz and yz
            anisotropy energies.
    Returns:
        theta:
            Polar angle in radians
        phi:
            Azimuthal angle in radians
    '''

    import os.path as op
    theta = 0
    phi = 0
    if op.isfile(fname):
        data = np.load(fname)
        DE = max(data['dE_zx'], data['dE_zy'])
        if DE > 0:
            theta = np.pi / 2
            if data['dE_zy'] > data['dE_zx']:
                phi = np.pi / 2
    return theta, phi


def spin_axis(fname='anisotropy_xy.npz') -> int:
    theta, phi = get_spin_direction(fname=fname)
    if theta == 0:
        return 2
    elif np.allclose(phi, np.pi / 2):
        return 1
    else:
        return 0
