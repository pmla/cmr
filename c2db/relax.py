# Creates: nm, fm, afm, relax-nm.traj, relax-fm.traj, relax-afm.traj

import argparse
import json
from pathlib import Path
import numpy as np
from ase.io import read, Trajectory
from ase.io.formats import UnknownFileTypeError
from ase.parallel import world, broadcast
from ase.build import niggli_reduce
from gpaw import GPAW, PW, FermiDirac, KohnShamConvergenceError

from c2db import readinfo, magnetic_atoms
from c2db.bfgs import BFGS
from c2db.references import formation_energy
from c2db.prepare import convert_to_canonical_structure
from c2db.dimensionality import is_2d


def relax_done(fname, emin=-np.inf):
    """Check if a relaxation is done"""
    if world.rank == 0:
        result = relax_done_master(fname, emin=emin)
    else:
        result = None
    return broadcast(result)


def relax_done_master(fname, fmax=0.01, smax=0.002, emin=-np.inf):
    if not Path(fname).is_file():
        return None, False

    try:
        slab = read(fname, parallel=False)
    except (IOError, UnknownFileTypeError):
        return None, False

    # some relaxations went wrong?
    assert abs(slab.cell[2, :2]).max() < 1e-12, slab.cell
    assert abs(slab.cell[:2, 2]).max() < 1e-12, slab.cell

    if slab.calc is None:
        return slab, False

    e = slab.get_potential_energy()
    f = slab.get_forces()
    s = slab.get_stress()

    done = e < emin or (f**2).sum(1).max() <= fmax**2 and abs(s).max() <= smax

    return slab, done


def relax(slab, tag, kptdens=6.0, width=0.05, emin=-np.inf):
    name = 'relax-' + tag

    params = Path('parameters.json')
    kwargs = dict(txt=name + '.txt',
                  mode=PW(800),
                  xc='PBE',
                  basis='dzp',
                  kpts={'density': kptdens, 'gamma': True},
                  occupations=FermiDirac(width=width),
                  poissonsolver={'dipolelayer': 'xy'})

    if params.is_file():
        # Read parameters from a file:
        kwargs2 = json.loads(params.read_text())
        kwargs.update(**kwargs2)
    slab.calc = GPAW(**kwargs)
    opt = BFGS(slab,
               logfile=name + '.log',
               trajectory=Trajectory(name + '.traj', 'a', slab))
    try:
        opt.run(fmax=0.01, smax=0.002, smask=[1, 1, 0, 0, 0, 0], emin=emin)
    except KohnShamConvergenceError:
        kwargs.update(kpts={'density': 9.0, 'gamma': True},
                      occupations=FermiDirac(width=0.02),
                      maxiter=999)
        slab.calc = GPAW(**kwargs)
        opt = BFGS(slab,
                   logfile=name + '.log',
                   trajectory=Trajectory(name + '.traj', 'a', slab))
        opt.run(fmax=0.01, smax=0.002, smask=[1, 1, 0, 0, 0, 0], emin=emin)


def relax_all(vacuum=7.5, prototype=None):
    """Relax atomic positions and unit cell.

    Different magnetic states will be tried: non-magnetic, ferro-magnetic, ...

    A gs.gpw file will be written for the most stable configuration.
    """

    info = readinfo()
    states = info.get('states')
    if states is None:
        states = ['nm', 'fm', 'afm']

    slab1, done1 = relax_done('relax-nm.traj')
    slab2, done2 = relax_done('relax-fm.traj')
    slab3, done3 = relax_done('relax-afm.traj')

    hform1 = np.inf
    hform2 = np.inf
    hform3 = np.inf

    # Non-magnetic:
    if 'nm' in states:
        if not done1:
            if slab1 is None:
                fnames = list(Path('.').glob('start.*'))
                assert len(fnames) == 1, fnames
                slab1 = read(str(fnames[0]))
                slab1.center(vacuum=vacuum, axis=2)
                slab1.pbc = (1, 1, 1)
                niggli_reduce(slab1)
                if prototype:
                    slab1 = convert_to_canonical_structure(slab1, prototype)

            slab1.pbc = (1, 1, 0)
            slab1._cell[:2, 2] = 0.
            slab1._cell[2, :2] = 0.
            slab1.set_initial_magnetic_moments(None)
            relax(slab1, 'nm')

        hform1 = formation_energy(slab1) / len(slab1)
        slab1.calc = None

    # Ferro-magnetic:
    if 'fm' in states:
        if slab2 is None:
            slab2 = slab1.copy()
            slab2.set_initial_magnetic_moments([1] * len(slab2))

        if not done2:
            relax(slab2, 'fm')

        magmom = slab2.get_magnetic_moment()
        if abs(magmom) > 0.1:
            hform2 = formation_energy(slab2) / len(slab2)
            # Create subfolder early so that fm-tasks can begin:
            if world.rank == 0 and not Path('fm').is_dir():
                Path('fm').mkdir()
        slab2.calc = None

    # Antiferro-magnetic:
    if 'afm' in states:
        if slab3 is None:
            if slab2 is None:
                # Special case.  Only afm relaxation is done
                fnames = list(Path('.').glob('start.*'))
                assert len(fnames) == 1, fnames
                slab3 = read(str(fnames[0]))
                slab3.center(vacuum=vacuum, axis=2)
                slab3.pbc = (1, 1, 1)
                niggli_reduce(slab3)
                slab3.pbc = (1, 1, 0)
                slab3._cell[2, :2] = 0.
                slab3._cell[:2, 2] = 0.
            else:
                slab3 = slab2.copy()
            magnetic = magnetic_atoms(slab3)
            nmag = sum(magnetic)
            if nmag == 2:
                magmoms = np.zeros(len(slab3))
                a1, a2 = np.where(magnetic)[0]
                magmoms[a1] = 1.0
                magmoms[a2] = -1.0
                slab3.set_initial_magnetic_moments(magmoms)
            else:
                done3 = True
                slab3 = None

        if not done3:
            relax(slab3, 'afm')

        if slab3 is not None:
            magmom = slab3.get_magnetic_moment()
            magmoms = slab3.get_magnetic_moments()
            if abs(magmom) < 0.02 and abs(magmoms).max() > 0.1:
                hform3 = formation_energy(slab3) / len(slab3)
            else:
                hform3 = np.inf
            slab3.calc = None

    hform = min(hform1, hform2, hform3)

    if hform1 > hform + 0.01:  # assume precison of 10 meV per atom
        hform1 = np.inf

    states = []
    for state, h, slab in [('nm', hform1, slab1),
                           ('fm', hform2, slab2),
                           ('afm', hform3, slab3)]:
        if h < np.inf:
            states.append(state)
            if is_2d(slab) and world.rank == 0 and not Path(state).is_dir():
                Path(state).mkdir()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=relax.__doc__)
    parser.add_argument('-p', '--prototype', help='One of MoS2, CdI2, ...')
    args = parser.parse_args()
    prototype = args.prototype

    if prototype is None:
        prototype = readinfo().get('prototype')

    if prototype and not Path('info.json').is_file():
        with open('info.json', 'w') as fd:
            fd.write(json.dumps({'prototype': prototype}))

    relax_all(prototype=prototype)
