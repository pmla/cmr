import sys
import os

from ase.db import connect


def add_experimental_bulk_parents(db, csvfile=None):
    import csv
    if csvfile is None:
        directory = os.path.dirname(__file__)
        filename = 'experimental_bulk_parents.csv'
    with open(os.path.join(directory, filename), 'r') as csvfile:
        csvfile.readline()  # skip header column
        reader = csv.reader(csvfile)
        for row in reader:
            uid, icsd_id, cod_id, monolayer_doi = row
            if uid.count('-') == 2:
                try:
                    ids = [db.get(uid=uid).id]
                except KeyError:
                    continue
            else:
                # experimental state missing
                ids = []
                for state in ['NM', 'FM', 'AFM']:
                    try:
                        id = db.get(uid=uid + '-' + state).id
                    except KeyError:
                        continue
                    except AssertionError:
                        prototype = uid.split('-')[1]
                        if prototype == "PbA2I4":
                            for row in db.select(uid=uid + '-' + state):
                                id = row.id
                                ids.append(id)
                        else:
                            raise
                    ids.append(id)
                # assert ids, row
                ids = list(set(ids))
            icsd_id = int(icsd_id) if icsd_id else None
            cod_id = int(cod_id) if cod_id else None
            dct = {name: value
                   for name, value in [('ICSD_id', icsd_id),
                                       ('COD_id', cod_id),
                                       ('monolayer_doi', monolayer_doi)]
                   if value}
            for id in ids:
                db.update(id, **dct)


def main(db_file):
    from c2db.check_duplicates import delete_duplicates
    from c2db.crosslinks import crosslink
    from c2db.prototypes import update_prototypes
    from c2db.strip_data import strip
    from c2db.test import test
    db = connect(db_file)

    print('Setting correct prototypes...')
    update_prototypes(db)

    print('Deleting duplicates...')
    path, ext = os.path.splitext(db_file)
    duplicate_db = connect(path + '_duplicates' + ext)
    delete_duplicates(db, duplicate_db)

    print('Add experimental bulk references')
    add_experimental_bulk_parents(db)

    print('Create crosslinks')
    db2 = connect('c2db-big-2.db')
    with db2:
        crosslink(db, db2)

    print('Run tests')
    db3 = connect('c2db-big.db')
    with db3:
        test(db2, db3)

    print('Strip off data')
    db4 = connect('c2db.db')
    with db4:
        strip(db3, db4)


if __name__ == '__main__':
    main(sys.argv[1])
