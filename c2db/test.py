"""Testing of db.

Run simple and fasts test to check data.
"""

from math import isnan
import numbers

from ase.db import connect

import numpy as np


def test_kvp(row):
    # Test that no numbers are extremely large or np.nan
    kvp = row.key_value_pairs.copy()
    name = '{}-{}-{}'.format(row.formula, row.prototype, row.magstate)

    def delete_key(key, msg=''):
        msg = 'DELETED {} {}={} {}'.format(name, key, kvp[key], msg)
        print(msg)
        del kvp[key]

    def warning(key, msg=''):
        if kvp.get('warning'):
            kvp['warning'] += ' {}'.format(msg)
        else:
            kvp['warning'] = msg
        msg = 'WARNING {} {}={} {}'.format(name, key, kvp[key], msg)
        print(msg)

    # Check for nan and >1e5 values
    for key, value in row.key_value_pairs.items():
        if key == "ICSD_id" or key == "COD_id":
            continue
        if isinstance(value, numbers.Number):
            if abs(value) > 1e5 or isnan(value):
                delete_key(key)

    # Determine if material is isotropic
    isotropic = False
    if 'spacegroup' in kvp:
        for char in kvp['spacegroup']:
            # If there is a >2 fold rotation => isotropic
            if char.isdigit() and int(char) > 2:
                isotropic = True

    # Check plasmafrequency and polarizability
    if 'plasmafrequency_x' in kvp:
        wpx = kvp['plasmafrequency_x']
        wpy = kvp['plasmafrequency_y']

        if (wpx < 0 or wpy < 0 or wpx > 100 or wpy > 100 or
            (isotropic and not np.allclose(wpx, wpy, rtol=0.05))):
            delete_key('plasmafrequency_x')
            delete_key('plasmafrequency_y')
    if 'alphax' in kvp:
        ax = kvp['alphax']
        ay = kvp['alphay']
        if isotropic and not np.allclose(ax, ay, rtol=0.05):
            print(row.prototype, row.formula, ax, ay)
            delete_key('alphax', msg='Not isotropic')
            delete_key('alphay', msg='Not isotropic')

    # Check band gaps
    if 'gap' in kvp:
        if 'gap_gw' in kvp and kvp['gap_gw'] < kvp['gap']:
            warning('gap_gw', msg='GW gap < PBE gap')
        if 'gap_hse' in kvp and kvp['gap_hse'] < kvp['gap']:
            warning('gap_hse', msg='HSE gap < PBE gap')

    # Check band edges
    if 'vbm' in kvp:
        if kvp['vbm'] > kvp['cbm']:
            delete_key('vbm')
            delete_key('cbm')

    if 'vbm_gw' in kvp:
        if kvp['vbm_gw'] > kvp['cbm_gw']:
            delete_key('vbm_gw')
            delete_key('cbm_gw')

    if 'vbm_hse' in kvp:
        if kvp['vbm_hse'] > kvp['cbm_hse']:
            delete_key('vbm_hse')
            delete_key('cbm_hse')

    # Check effective masses
    for key in ['emass1', 'emass2', 'emass2_1', 'emass2_2',
                'hmass1', 'hmass2', 'hmass2_1', 'hmass2_2',
                'emass1_nosoc', 'emass2_nosoc', 'hmass1_nosoc',
                'hmass2_nosoc']:

        if key not in kvp:
            continue

        if kvp[key] < 0:
            delete_key(key)
        elif 100 < kvp[key]:
            delete_key(key, msg='{} is unphysical'.format(key))

    # Check magnetic anisotropy
    for key in ['maganis_zx', 'maganis_zy']:
        if key not in kvp:
            continue
        normmaganis = abs(kvp[key] / kvp['cell_area'])
        if abs(normmaganis) > 50:
            delete_key(key, msg='{} large magnetic anisotropy')
        elif abs(normmaganis) > 10:
            warning(key, msg='{} is unusually large'.format(key))

    # Check elastic tensor
    if 'c_11' in kvp and (kvp['c_11'] < 0 or kvp['c_22'] < 0):
        delete_key('c_11', msg='A negative number occurred')
        delete_key('c_22', msg='A negative number occurred')
        delete_key('c_12', msg='A negative number occurred')

    if isotropic and 'c_11' in kvp and 'c_22' in kvp:
        if not np.allclose(kvp['c_11'], kvp['c_22'], rtol=0.1):
            delete_key('c_11', msg='Elastic tensor not isotropic')
            delete_key('c_22', msg='Elastic tensor not isotropic')
            delete_key('c_12', msg='Elastic tensor not isotropic')

    for key in ['c_11', 'c_22']:
        if key not in kvp:
            continue
        if kvp[key] > 1000:
            delete_key(key)

    # Check dos
    if 'is_metallic' in kvp and kvp['is_metallic']:
        if 'dosef_nosoc'in kvp and kvp['dosef_nosoc'] < 0.01:
            warning('dosef_nosoc', msg='metallic but dosef very small')

    if 'minhessianeig' in kvp and kvp['minhessianeig'] > 0.01:
        warning('minhessianeig', msg='hessian should be <=0')

    # Check deformation potentials
    for key in ['D_vbm_nosoc', 'D_cbm_nosoc', 'D_vbm', 'D_cbm']:
        if key not in kvp:
            continue
        if abs(kvp[key]) > 50:
            delete_key(key)

    return kvp


def test(db1, db2):
    for row in db1.select():
        kvp = test_kvp(row)

        # Write new database with updated values
        try:
            db2.write(row.toatoms(), data=row.data, **kvp)
        except AttributeError:
            db2.write(row.toatoms(), **kvp)


if __name__ == '__main__':
    # Usage python -m c2db.test old.db new.db

    import sys
    db1 = connect(sys.argv[1])
    with connect(sys.argv[2]) as db2:
        test(db1, db2)
