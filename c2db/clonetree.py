"""Tool for copying or symlinking a tree of files."""
import argparse
from pathlib import Path
from typing import List, Tuple


p = argparse.ArgumentParser()
p.add_argument('-z', '--dry-run', action='store_true')
p.add_argument('-v', '--verbose', action='store_true')
sp = p.add_subparsers(dest='command')

help = """Copy all files from source/ folder and
its subfolders to destination/source/."""
p1 = sp.add_parser('copy', help=help, description=help)
p1.add_argument('source')
p1.add_argument('destination')

help = """Create symlinks from destination/source/ and
its subfolders to source/ folder.  Symlinks will only
be created for the specified filenames."""
p2 = sp.add_parser('symlink', help=help, description=help)
p2.add_argument('source')
p2.add_argument('destination')
p2.add_argument('filenames', nargs='+', metavar='filename')

args = p.parse_args()

if not args.command:
    p.print_help()
else:
    source = Path(args.source)
    destination = Path(args.destination)

if args.command == 'copy':
    copy: List[Tuple[Path, Path]] = []
    for srcdir in source.glob('**/'):
        destdir = destination / srcdir.relative_to(source.parent)
        assert destdir.is_dir(), destdir
        for srcfile in srcdir.glob('*'):
            if srcfile.is_file() and not srcfile.is_symlink():
                destfile = destdir / srcfile.name
                assert not destfile.is_file(), destfile
                copy.append((srcfile, destfile))
    if args.dry_run:
        if args.verbose:
            for srcfile, destfile in copy:
                print(f'Copy {srcfile} to {destfile}')
        print(f'Would copy {len(copy)} files')
    else:
        print(f'Copying {len(copy)} files')
        for srcfile, destfile in copy:
            destfile.write_bytes(srcfile.read_bytes())

elif args.command == 'symlink':
    filenames = set(args.filenames)
    mkdir: List[Path] = []

    symlink: List[Tuple[Path, Path]] = []
    for srcdir in source.glob('**/'):
        destdir = destination / srcdir.relative_to(source.parent)
        if not destdir.is_dir():
            mkdir.append(destdir)
        for srcfile in srcdir.glob('*'):
            if srcfile.name in filenames:
                destfile = destdir / srcfile.name
                assert not destfile.is_file(), destfile
                symlink.append((srcfile, destfile))
    if args.dry_run:
        if args.verbose:
            for destdir in mkdir:
                print(f'New folder: {destdir}')
            for srcfile, destfile in symlink:
                print(f'Symlink: {destfile} -> {srcfile}')
        print(f'Would create {len(mkdir)} folders and '
              f'symlink {len(symlink)} files')
    else:
        print(f'Creating {len(mkdir)} folders and '
              f'symlinking {len(symlink)} files')
        for destdir in mkdir:
            destdir.mkdir()
        for srcfile, destfile in symlink:
            destfile.symlink_to(srcfile.resolve())
