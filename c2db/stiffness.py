# Creates: strain_quantities.npz

import numpy as np
from ase.parallel import rank
from c2db.strains import calculate_2D_stiffness_tensor

if __name__ == '__main__':
    out_archive = 'strain_quantities.npz'

    try:
        data = dict(np.load(out_archive))
    except IOError:
        data = {}
    if 'stiffness_tensor' not in data:
        data = calculate_2D_stiffness_tensor(data=data)

        if rank == 0:
            np.savez(out_archive, **data)
