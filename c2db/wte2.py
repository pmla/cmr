"""T' phase (WTe2)."""

from pathlib import Path
from ase.db import connect
from ase.io import read
from ase.build import mx2


def create_folders():
    db = connect('c2db.db')
    for row in db.select(prototype='CdI2'):
        a = row.toatoms()
        print(row.formula)
        a = a.repeat((1, 2, 1))
        a.cell[1] += a.cell[0]
        a.positions[3:] += a.cell[0]
        a.positions[0, 1] += 0.1
        a.positions[3, 1] -= 0.1
        Path(row.formula + '-WTe2').mkdir()
        a.calc = None
        a.write(row.formula + '-WTe2/start.traj')
        info = '{"prototype": "WTe2"}\n'
        Path(row.formula + '-WTe2/info.json').write_text(info)


def check(fname):
    slab = read(fname)
    a = slab.cell[0, 0]
    b = slab.cell[1, 1]
    P = slab.positions
    d1 = a * 3**0.5 - b
    ref = mx2(kind='1T', a=a, thickness=slab.positions[:, 2].ptp())
    ref = ref.repeat((1, 2, 1))
    ref.positions[3:] += ref.cell[0]
    D = slab.positions - ref.positions
    D -= D[0]
    d2 = abs(D).max()
    d3 = b - P[3, 1] - (P[3, 1] - P[0, 1])
    return abs(d1), d2, abs(d3), slab.get_potential_energy()


if __name__ == '__main__':
    import sys
    for fname in sys.argv[1:]:
        a, b = fname.rsplit('/', 1)
        d1, d2, d3 = check('{}/relax-{}.traj'.format(a, b))
        if d1 > 0.03 or d2 > 0.02 or d3 > 0.01:
            print(fname)
