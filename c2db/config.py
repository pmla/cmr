"""Test configuration.

Adjusting paths to c2db.db, custom.py and the static folder with
png and csv files.  Then, run like this::

    $ ASE_DB_APP_CONFIG=~/cmr/c2db/config.py python3 -m ase.db.app

"""

from pathlib import Path

home = Path.home() / 'cmr' / 'c2db'
ASE_DB_NAMES = [home / 'c2db.db', home / 'custom.py']
ASE_DB_HOMEPAGE = 'https://cmr.fysik.dtu.dk/'
ASE_DB_FOOTER = 'Testing ...'
ASE_DB_TMPDIR = home / 'static'
ASE_DB_DOWNLOAD = False
