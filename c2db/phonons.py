# Creates: phonon.*.pckl

import pickle
from pathlib import Path

import numpy as np
import ase.units as units
import ase.io.ulm as ulm
from ase.parallel import world
from ase.io import read
from ase.phonons import Phonons
from gpaw import GPAW


def phonons():
    state = Path().cwd().parts[-1]

    # Remove empty files:
    if world.rank == 0:
        for f in Path().glob('phonon.*.pckl'):
            if f.stat().st_size == 0:
                f.unlink()
    world.barrier()

    params = {'symmetry': {'point_group': False}}
    name = '../relax-{}.traj'.format(state)
    u = ulm.open(name)
    params.update(u[-1].calculator.parameters)
    u.close()

    slab = read(name)
    calc = GPAW(txt='phonons.txt', **params)

    p = Phonons(slab, calc, supercell=(2, 2, 1))
    p.run()
    # p.read()
    # e, m = p.band_structure([[0.5, 0, 0]], modes=True)


def analyse(atoms, name='phonon', delta=0.01, D=3):
    N = len(atoms)
    C = np.empty((2, 2, N, D, 2, 2, N, D))
    for a in range(N):
        for i, v in enumerate('xyz'[:D]):
            forces = []
            for sign in '-+':
                filename = '{}.{}{}{}.pckl'.format(name, a, v, sign)
                with open(filename, 'rb') as fd:
                    f = pickle.load(fd)[:, :D]
                    f[a] -= f.sum(axis=0)
                    f.shape = (2, 2, N, D)
                    forces.append(f)
            C[0, 0, a, i] = (forces[0] - forces[1]) / (2 * delta)

    C[0, 1] = C[0, 0, :, :, :, ::-1]
    C[1, 0] = C[0, 0, :, :, ::-1]
    C[1, 1] = C[0, 0, :, :, ::-1, ::-1]

    C.shape = (4 * D * N, 4 * D * N)
    C += C.T.copy()
    C *= 0.5

    # Mingo correction.
    #
    # See:
    #
    #    Phonon transmission through defects in carbon nanotubes
    #    from first principles
    #
    #    N. Mingo, D. A. Stewart, D. A. Broido, and D. Srivastava
    #    Phys. Rev. B 77, 033418 – Published 30 January 2008
    #    http://dx.doi.org/10.1103/PhysRevB.77.033418

    R_in = np.zeros((4 * D * N, D))
    for n in range(D):
        R_in[n::D, n] = 1.0
    a_in = -np.dot(C, R_in)
    B_inin = np.zeros((4 * D * N, D, 4 * D * N, D))
    for i in range(4 * D * N):
        B_inin[i, :, i] = np.dot(R_in.T, C[i, :, np.newaxis]**2 * R_in) / 4
        for j in range(4 * D * N):
            B_inin[i, :, j] += np.outer(R_in[i], R_in[j]).T * C[i, j]**2 / 4
    # L_in = np.linalg.solve(B_inin.reshape((36 * N, 36 * N)),
    #                        a_in.reshape((36 * N,))).reshape((12 * N, D))
    L_in = np.dot(np.linalg.pinv(B_inin.reshape((4 * D**2 * N, 4 * D**2 * N))),
                  a_in.reshape((4 * D**2 * N,))).reshape((4 * D * N, D))
    D_ii = C**2 * (np.dot(L_in, R_in.T) + np.dot(L_in, R_in.T).T) / 4
    # print('Small number:', (D_ii**2 / C**2).max())
    C += D_ii

    # Conversion factor: sqrt(eV / Ang^2 / amu) -> eV
    s = units._hbar * 1e10 / (units._e * units._amu)**0.5
    M = np.repeat(atoms.get_masses(), D)
    invM = np.outer(M, M)**-0.5
    eigs = []
    freqs = []
    C.shape = (2, 2, N * D, 2, 2, N * D)
    for q in [(0, 0), (0, 1), (1, 0), (1, 1)]:
        K = np.zeros((N * D, N * D))
        for c1 in range(2):
            for c2 in range(2):
                K += (-1)**np.dot(q, (c1, c2)) * C[0, 0, :, c1, c2]
        e = np.linalg.eigvalsh(K)
        f2 = np.linalg.eigvalsh(invM * K)
        f = abs(f2)**0.5 * np.sign(f2) * s
        eigs.append(e)
        freqs.append(f)

    return np.array(eigs), np.array(freqs)


if __name__ == '__main__':
    phonons()
