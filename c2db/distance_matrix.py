# Creates: distances_split.npz

import numpy as np
from itertools import permutations, product
from ase.build import niggli_reduce
from c2db.rmsd_monolayer import calculate_rmsd
from fractions import gcd
from c2db.utils import get_reduced_formula
import os


def scale_invariant_metric(i, j, zscale=False):
    rmsd, __, ___ = calculate_rmsd(i, j, zscale=zscale)
    # unit_cell_distance = calculate_unit_cell_difference(i, j)
    return rmsd  # + unit_cell_distance


def permute_classes(i, j):

    def classes_split_by_count(atoms):
        indices, count = get_occurrences(atoms.get_chemical_symbols())
        multiplicities = list(set(count))
        classes = {}
        for multiplicity in multiplicities:
            classes[multiplicity] = [indices[x] for x in
                                     np.where(count == multiplicity)[0]]
        return classes

    classes_i = classes_split_by_count(i)
    classes_j = classes_split_by_count(j)
    foo = [[permutations(val), key] for key, val in classes_j.items()]
    permutation_list = [x[0] for x in foo]
    count_indices = [x[1] for x in foo]
    symbols = i.get_chemical_symbols()
    for permutation in product(*permutation_list):
        mapping = {}
        for count_index, element in zip(count_indices, permutation):
            for symbol_i, symbol_j in zip(classes_i[count_index], element):
                mapping[symbol_i] = symbol_j
        new_symbols = [mapping[x] for x in symbols]
        yield new_symbols


def get_occurrences(numbers):
    idx = 0
    unique_numbers = {}
    counter = []
    for number in numbers:
        if number in unique_numbers:
            counter[unique_numbers[number]] += 1
        else:
            unique_numbers[number] = idx
            counter.append(1)
            idx += 1
    return (dict(zip(unique_numbers.values(), unique_numbers.keys())),
            np.array(counter))


def lcm(a, b):
    return a * b / gcd(a, b)


def find_square(n):
    """Returns the two numbers closest to one another whose product is n"""
    test = int(np.sqrt(n))
    while test > 1:
        if n % test == 0:
            return sorted([n // test, test])[::-1]
        test -= 1
    return n, 1


def get_distance(i, j, zscale=False, permute_atoms=True,
                 metric=scale_invariant_metric):
    unique_i, count_i = get_occurrences(i.get_chemical_symbols())
    unique_j, count_j = get_occurrences(j.get_chemical_symbols())
    n_i = sum(count_i)
    n_j = sum(count_j)
    if n_i != n_j:
        new_n = lcm(n_i, n_j)
        repeat_i = int(new_n / n_i)
        repeat_j = int(new_n / n_j)
        if repeat_i != 1:
            repeat_x, repeat_y = find_square(repeat_i)
            i = i.repeat((repeat_x, repeat_y, 1))
            count_i *= repeat_i
        if repeat_j != 1:
            repeat_x, repeat_y = find_square(repeat_j)
            j = j.repeat((int(repeat_x), int(repeat_y), 1))
            count_j *= repeat_j
    if tuple(count_i) not in permutations(count_j):
        # Different stoichiometries
        return np.inf
    else:
        min_distance = np.inf
        new_i = i.copy()
        new_j = j.copy()
        if permute_atoms:
            symbol_permutations = permute_classes(i, j)
        else:
            symbol_permutations = [i.get_chemical_symbols()]
        for symbol_permutation in symbol_permutations:
            new_i.set_chemical_symbols(symbol_permutation)
            new_i.center(axis=2)
            new_j.center(axis=2)
            new_i.pbc = [1, 1, 1]
            new_j.pbc = [1, 1, 1]
            niggli_reduce(new_i)
            niggli_reduce(new_j)
            for x_sign in ([1, -1]):
                new_i.positions[:, 0] *= x_sign
                new_i.center(axis=0)
                for y_sign in ([1, -1]):
                    new_i.positions[:, 1] *= y_sign
                    new_i.center(axis=0)
                    for z_sign in ([1, -1]):
                        new_i.positions[:, 2] *= z_sign
                        new_i.center(axis=2)
                        d = metric(new_i, new_j, zscale=zscale)
                        if d < min_distance:
                            min_distance = d
        return min_distance


def split_distance(distance_matrix, row_formulas, row_ids, row_labels):
    while True:
        bad_rows = np.where(distance_matrix[0] == np.inf)[0]
        good_rows = np.where(distance_matrix[0] != np.inf)[0]
        yield (distance_matrix[good_rows][:, good_rows],
               row_formulas[good_rows],
               row_ids[good_rows],
               row_labels[good_rows])
        if bad_rows.size == 0:
            break
        distance_matrix = distance_matrix[bad_rows][:, bad_rows]
        row_formulas = row_formulas[bad_rows]
        row_ids = row_ids[bad_rows]
        row_labels = row_labels[bad_rows]


def calculate_distance_matrix(database,
                              outfile=None,
                              ignore_stoichiometry=False,
                              stoichiometry=None,
                              zscale=False):
    if outfile is None:
        if ignore_stoichiometry:
            outfile = 'distances_collapsed_split.npz'
        else:
            outfile = 'distances_split.npz'

    if not os.path.isfile(outfile):
        conn = connect(database)
        if stoichiometry is None:
            rows = [row for row in conn.select() if len(row.toatoms()) <= 50]
        else:
            rows = [row for row in conn.select()
                    if (len(row.toatoms()) <= 50 and
                        get_reduced_formula(row.formula, stoichiometry=True)
                        == stoichiometry)]
        structures = [row.toatoms() for row in rows]
        if ignore_stoichiometry:
            for structure in structures:
                structure.numbers = [0] * len(structure)
        formulas = np.array([row.formula for row in rows])
        n = len(structures)
        print(n, flush=True)
        distance_matrix = np.zeros((n, n))
        for i in range(n):
            print(i, flush=True)
            apply_scaling_i = True
            z_variation_i = np.diff(structures[i].positions[:, 2])
            if (z_variation_i == 0).all():
                apply_scaling_i = False
            for j in range(i + 1, n):
                apply_scaling_j = True
                if apply_scaling_i:
                    z_variation_j = np.diff(structures[j].positions[:, 2])
                    if (z_variation_j == 0).all():
                        apply_scaling_j = False
                scale = zscale and apply_scaling_i and apply_scaling_j
                d = get_distance(structures[i], structures[j],
                                 zscale=scale)
                distance_matrix[i, j] = d
                distance_matrix[j, i] = d
        print('Calculated distance matrix!', flush=True)
        prototypes = []
        ids = []
        for row in rows:
            ids.append(row.id)
            try:
                prototypes.append(row.prototype)
            except AttributeError:
                print(row.formula, 'has unknown prototype', flush=True)
                prototypes.append('unknown')
        ids = np.array(ids)
        prototypes = np.array(prototypes)
        result = {}
        split = split_distance(distance_matrix, formulas, prototypes, ids)
        for distance_matrix, formulas, prototypes, ids in split:
            stoichiometry = get_reduced_formula(formulas[0],
                                                stoichiometry=True)
            result[stoichiometry] = {'distance_matrix': distance_matrix,
                                     'formulas': formulas,
                                     'ids': ids,
                                     'prototypes': prototypes}
            np.savez(outfile,
                     **result)
    d = np.load(outfile)
    result = {}
    for key in d.keys():
        result[key] = d[key][()]
    return result


if __name__ == "__main__":
    from ase.db import connect
    import sys
    db = sys.argv[1]
    try:
        stoichiometry = sys.argv[2]
    except IndexError:
        stoichiometry = None
    try:
        outfile = sys.argv[3]
    except IndexError:
        outfile = None
    calculate_distance_matrix(db, outfile=outfile, stoichiometry=stoichiometry)
