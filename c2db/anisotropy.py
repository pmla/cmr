# Creates: anisotropy_zy.npz
import os.path as op

import numpy as np
from gpaw import GPAW
import gpaw.mpi as mpi
from gpaw.spinorbit import get_anisotropy
from ase.parallel import paropen, parprint


def anisotropy(kptpath=None, width=0.001, nbands=None):
    """Calculate the magnetic anisotropy from densk.gpw."""

    if not op.isfile('densk.gpw'):
        return

    ranks = [0]
    comm = mpi.world.new_communicator(ranks)
    if mpi.world.rank in ranks:
        calc = GPAW('densk.gpw', communicator=comm, txt=None)
        if calc.wfs.nspins == 1:
            parprint("The ground state is nonmagnetic. "
                     "Skipping anisotropy calculation")
            return
        E_x = get_anisotropy(calc, theta=np.pi / 2, nbands=nbands, width=width)
        calc = GPAW('densk.gpw', communicator=comm, txt=None)
        E_z = get_anisotropy(calc, theta=0.0, nbands=nbands, width=width)
        calc = GPAW('densk.gpw', communicator=comm, txt=None)
        E_y = get_anisotropy(
            calc, theta=np.pi / 2, phi=np.pi / 2, nbands=nbands, width=width)

        dE_zx = E_z - E_x
        dE_zy = E_z - E_y

        with paropen('anisotropy_xy.npz', 'wb') as f:
            np.savez(f, dE_zx=dE_zx, dE_zy=dE_zy)


if __name__ == '__main__':
    from c2db import run
    run(anisotropy)
