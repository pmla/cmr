# Creates: bse_pol_par.csv, bse_pol_perp.csv, eig_par.dat, eig_perp.dat
# temporary: gs_bse.gpw, gs_nosym.gpw, wfile.npz
import os

import numpy as np
from gpaw import GPAW
from gpaw.occupations import FermiDirac
from gpaw.response.bse import BSE
import gpaw.mpi as mpi


def bse(kptdensity=20, ecut=50.0, num_v=4, num_c=4):
    """Calculate the bse absorption spectrum. This kpt-density yields
    48x48 for MoS2-H"""

    def get_kpts_size(atoms, density):
        """trying to get a reasonable monkhorst size which hits high
        symmetry points
        """
        from gpaw.kpt_descriptor import kpts2sizeandoffsets as k2so
        size, offset = k2so(atoms=atoms, density=density)
        size[2] = 1
        for i in range(2):
            if size[i] % 6 != 0:
                size[i] = 6 * (size[i] // 6 + 1)
        kpts = {'size': size, 'gamma': True}
        return kpts

    calc = GPAW('gs.gpw', txt=None)
    nv = int(calc.get_number_of_electrons() / 2)
    nbands = 6 * nv
    kpts = get_kpts_size(atoms=calc.atoms, density=kptdensity)
    calc.set(kpts=kpts,
             fixdensity=True,
             txt='gs_bse.txt',
             maxiter=1000,
             occupations=FermiDirac(width=1.0e-6),
             nbands=int(nbands * 1.5),
             convergence={'bands': nbands})
    calc.get_potential_energy()
    calc.write('gs_bse.gpw', mode='all')

    spin = calc.get_spin_polarized()
    if spin:
        f0 = calc.get_occupation_numbers(spin=0)
        f1 = calc.get_occupation_numbers(spin=1)
        n0 = np.where(f0 < 1.0e-6)[0][0]
        n1 = np.where(f1 < 1.0e-6)[0][0]
        valence_bands = [range(n0 - num_v, n0), range(n1 - num_v, n1)]
        conduction_bands = [range(n0, n0 + num_c), range(n1, n1 + num_c)]
    else:
        valence_bands = range(nv - 4, nv)
        conduction_bands = range(nv, nv + 4)

    bse = BSE('gs_bse.gpw',
              spinors=True,
              ecut=ecut,
              valence_bands=valence_bands,
              conduction_bands=conduction_bands,
              nbands=nbands,
              mode='BSE',
              wfile='wfile',
              truncation='2D',
              txt='bse.txt')

    w_w = np.linspace(0.0, 5.0, 5001)

    bse.get_polarizability(eta=0.05,
                           filename='bse_pol_par.csv',
                           direction=0,
                           write_eig='eig_par.dat',
                           pbc=[True, True, False],
                           w_w=w_w)

    bse.get_polarizability(eta=0.05,
                           filename='bse_pol_perp.csv',
                           direction=2,
                           write_eig='eig_perp.dat',
                           pbc=[True, True, False],
                           w_w=w_w)

    if mpi.world.rank == 0:
        os.system('rm gs_bse.gpw')
        os.system('rm gs_nosym.gpw')
        os.system('rm wfile.npz')


def bse_old():
    import shutil
    from ase.io import read
    path = os.path.basename(os.getcwd())
    formula, prototype = path.split('-')
    try:
        atoms = read('../BSE/{}.xyz'.format(formula))
    except FileNotFoundError:
        return

    dx = atoms.positions[1:, 0].ptp()
    if dx < 0.01:
        prot = 'MoS2'
    else:
        prot = 'CdI2'
    if prot == prototype:
        shutil.copy('../BSE/abs_perp_{}.csv'.format(formula),
                    'bse_abs_perp.csv'.format(formula))
        shutil.copy('../BSE/abs_par_{}.csv'.format(formula),
                    'bse_abs_par.csv'.format(formula))


if __name__ == '__main__':
    bse()
