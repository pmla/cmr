from pathlib import Path
from gpaw import GPAW
from c2db import chdir
import shutil
import sys


tasks = [('gs', 'gs.gpw'),
         ('stability', 'relax-spin-polarized_2x2.traj'),
         ('ehmasses', 'masstensor.npz'),
         ('fermi_surface', 'fermi_surface.npz'),
         ('bandstructure', 'eigs_spinorbit.npz'),
         ('polarizability', 'polarizability_tetra.npz'),
         ('strains', 'strain_quantities.npz'),
         ('hse', 'hse_bandstructure3.npz'),
         ('pdos', 'pdos.json'),
         ('gllbsc', 'gllbsc.json'),
         ('anisotropy', 'anisotropy.npz'),
         ('bse', 'bse_abs_par.csv'),
         ('gw', 'gw.npz')]


def line(t, status='done'):
    return 'xxxx-xx-xx-xx:xx:xx {} c2db.{}'.format(status, t)


rename = [('relax-spin-paired.traj', 'relax-nm.traj'),
          ('relax-spin-paired.txt', 'relax-nm.txt'),
          ('relax-spin-paired.log', 'relax-nm.log'),
          ('relax-spin-polarized.traj', 'relax-fm.traj'),
          ('relax-spin-polarized.txt', 'relax-fm.txt'),
          ('relax-spin-polarized.log', 'relax-fm.log')]

do_not_move = {b for a, b in rename}
do_not_move.add('info.json')

for folder in sys.argv[1:]:
    print(folder)
    with chdir(folder):
        for a, b in rename:
            if Path(a).is_file() and not Path(b).is_file():
                Path(a).rename(b)

        if Path('gs.gpw').is_file():
            calc = GPAW('gs.gpw', txt=None)
            natoms = len(calc.atoms.numbers)
            if calc.get_number_of_spins() == 2:
                state = 'fm'
                if abs(calc.get_magnetic_moment()) / natoms < 0.1:
                    state = 'afm'
            else:
                state = 'nm'

            Path(state).mkdir()
            for x in Path('.').glob('*'):
                if x.name not in do_not_move and x.is_file():
                    shutil.move(x.name, '{}/{}'.format(state, x.name))

            with chdir(state):
                with open('.tasks', 'w') as fd:
                    for t, fname in tasks:
                        if Path(fname).is_file():
                            print(line(t), file=fd)
