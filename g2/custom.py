title = 'G2/97'

default_columns = ['id', 'age', 'formula', 'energy', 'fmax', 'pbc', 'volume', 'charge', 'mass', 'name', 'relativistic']
